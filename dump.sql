CREATE DATABASE  IF NOT EXISTS `k165810_SeeTheBee` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `k165810_SeeTheBee`;
-- MariaDB dump 10.19  Distrib 10.4.20-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: k165810_seethebee
-- ------------------------------------------------------
-- Server version	10.4.20-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `b_behandlungen`
--

DROP TABLE IF EXISTS `b_behandlungen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_behandlungen` (
  `b_id` int(11) NOT NULL AUTO_INCREMENT,
  `b_datevon` date NOT NULL,
  `b_behandlung` enum('Wärmebehandlung','Entnahme von Drohnenbrut','Bannwabenverfahren','Ablegerbildung','Entsorgen der Brutwaben','organische Phosphorsäureester','Pyrethroide','Metarhizium-Pilz','Bücherskorpion','Milchsäure sprühen','ätherische Öle','Ameisensäure Schwammtuch','Ameisensäure Universal-Verdunster','Ameisensäure Liebig-Verdunster','Ameisensäure Nassenheider-Verdunster','Oxalsäure träufeln','Oxalsäure sprühen','Oxalsäure E-Verdampfer mit Thermostat','Oxalsäure E-Verdampfer ohne Thermostat','Oxalsäure Gas-Verdampfer','Oxalsäure Vernebler','natürliche Evolution (ohne Fremdeinwirkung)') NOT NULL,
  `b_infos` varchar(100) DEFAULT NULL,
  `b_stoecke` int(11) NOT NULL,
  `b_datebis` date DEFAULT NULL,
  `b_von` varchar(45) DEFAULT NULL,
  `b_bis` varchar(45) DEFAULT NULL,
  `b_user` varchar(50) NOT NULL,
  `b_standorte` int(11) NOT NULL,
  PRIMARY KEY (`b_id`),
  KEY `fk_b_behandlungen_s_stoecke1_idx` (`b_stoecke`),
  KEY `fk_b_behandlungen_u_user1_idx` (`b_user`),
  KEY `fk_b_behandlungen_st_standorte1_idx` (`b_standorte`),
  CONSTRAINT `fk_b_behandlungen_s_stoecke1` FOREIGN KEY (`b_stoecke`) REFERENCES `s_stoecke` (`s_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_b_behandlungen_st_standorte1` FOREIGN KEY (`b_standorte`) REFERENCES `st_standorte` (`st_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_b_behandlungen_u_user1` FOREIGN KEY (`b_user`) REFERENCES `u_user` (`u_name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `b_behandlungen`
--

LOCK TABLES `b_behandlungen` WRITE;
/*!40000 ALTER TABLE `b_behandlungen` DISABLE KEYS */;
INSERT INTO `b_behandlungen` VALUES (4,'2021-12-08','Ameisensäure Universal-Verdunster','',6,'2021-12-12','21:38','12:00','elaine125',5),(5,'2022-01-07','Ameisensäure Liebig-Verdunster','',6,'2022-01-10','22:09','23:09','elaine125',5),(6,'2022-01-20','Entsorgen der Brutwaben','Viel Schaden am Stock',13,'2022-01-22','22:26','04:26','elaine125',6),(7,'2021-12-28','Ablegerbildung','',6,'2022-01-03','16:27','14:22','elaine125',5),(8,'2022-01-23','Bannwabenverfahren','kein Abfall bei den Stöcken',16,'2022-01-25','15:53','22:56','elaine124',4),(9,'2022-01-08','Oxalsäure träufeln','',16,'2022-01-11','22:55','15:55','elaine124',4),(10,'2022-02-01','Ablegerbildung','',16,'2022-02-06','22:57','08:57','elaine124',4),(11,'2022-01-22','organische Phosphorsäureester','',16,'2022-01-23','10:05','23:05','elaine124',4);
/*!40000 ALTER TABLE `b_behandlungen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `e_eintraege`
--

DROP TABLE IF EXISTS `e_eintraege`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `e_eintraege` (
  `e_id` int(11) NOT NULL AUTO_INCREMENT,
  `e_date` date NOT NULL,
  `e_honigw` int(11) DEFAULT NULL,
  `e_brutw` int(11) DEFAULT NULL,
  `e_pollenw` int(11) DEFAULT NULL,
  `e_drohnenw` int(11) DEFAULT NULL,
  `e_leerw` int(11) DEFAULT NULL,
  `e_mittelw` int(11) DEFAULT NULL,
  `e_koenigin` tinyint(4) DEFAULT NULL,
  `e_propolis` enum('1','2','3','4') DEFAULT NULL,
  `e_wirrbau` enum('1','2','3','4') DEFAULT NULL,
  `e_stetigkeit` enum('1','2','3','4') DEFAULT NULL,
  `e_sanftmut` enum('1','2','3','4') DEFAULT NULL,
  `e_weiselz` int(11) DEFAULT NULL,
  `e_varroa` int(11) DEFAULT NULL,
  `e_infos` varchar(100) DEFAULT NULL,
  `e_stoecke` int(11) DEFAULT NULL,
  `e_von` varchar(45) DEFAULT NULL,
  `e_bis` varchar(45) DEFAULT NULL,
  `e_user` varchar(50) DEFAULT NULL,
  `e_standorte` int(11) DEFAULT NULL,
  PRIMARY KEY (`e_id`),
  KEY `fk_e_eintraege_s_stoecke1_idx` (`e_stoecke`),
  KEY `fk_e_eintraege_st_standorte1_idx` (`e_standorte`),
  KEY `fk_e_eintraege_u_user1_idx` (`e_user`),
  CONSTRAINT `fk_e_eintraege_s_stoecke1` FOREIGN KEY (`e_stoecke`) REFERENCES `s_stoecke` (`s_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_e_eintraege_st_standorte1` FOREIGN KEY (`e_standorte`) REFERENCES `st_standorte` (`st_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_e_eintraege_u_user1` FOREIGN KEY (`e_user`) REFERENCES `u_user` (`u_name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `e_eintraege`
--

LOCK TABLES `e_eintraege` WRITE;
/*!40000 ALTER TABLE `e_eintraege` DISABLE KEYS */;
INSERT INTO `e_eintraege` VALUES (7,'2021-12-02',5,3,3,0,1,2,1,'3','1','2','3',0,0,'',6,'12:20','13:00','elaine125',5),(8,'2021-12-06',6,4,2,0,1,1,0,'2','2','3','3',0,0,'',6,'10:30','10:45','elaine125',5),(9,'2021-12-13',5,3,3,0,0,2,0,'2','1','3','4',0,0,'',6,'17:35','17:50','elaine125',5),(10,'2021-12-26',4,3,2,0,0,1,1,'3','1','3','3',0,0,'',6,'15:15','15:55','elaine125',5),(12,'2022-01-20',0,0,0,0,0,0,1,'3','1','2','3',0,0,'',6,'21:45','','elaine125',5),(13,'2021-12-27',5,4,3,0,0,0,1,'3','1','1','3',0,0,'Zuckerwasser gefüttert',10,'16:53','17:07','elaine125',6),(14,'2022-01-05',5,4,3,0,0,0,0,'2','1','1','2',0,35,'',13,'13:08','15:15','elaine125',6),(15,'2022-01-20',10,6,3,1,1,0,1,'2','1','1','1',0,45,'Zuckerwasser',13,'22:25','','elaine125',6),(16,'2022-01-04',4,2,1,0,1,1,0,'3','1','2','2',0,32,'',10,'12:30','12:56','elaine125',6),(17,'2022-01-04',5,1,2,0,1,0,1,'3','3','2','3',0,35,'',16,'17:48','18:05','elaine124',4),(18,'2022-01-20',3,3,2,0,1,0,0,'3','3','2','3',1,25,'Zuckerwasser gefüttert',16,'22:55','01:55','elaine124',4),(19,'2022-01-20',7,4,5,0,0,3,0,'3','1','1','1',0,0,'',16,'22:56','','elaine124',4),(20,'2022-01-20',4,3,3,1,0,1,1,'3','3','2','3',0,0,'',16,'23:06','','elaine124',4),(21,'2022-01-21',4,0,1,0,0,0,0,'3','1','1','3',0,0,'',17,'10:32','','elaine124',4);
/*!40000 ALTER TABLE `e_eintraege` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `es_einstellungen`
--

DROP TABLE IF EXISTS `es_einstellungen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `es_einstellungen` (
  `es_propolis` tinyint(1) NOT NULL DEFAULT 1,
  `es_wirrbau` tinyint(1) NOT NULL DEFAULT 1,
  `es_stetigkeit` tinyint(1) NOT NULL DEFAULT 1,
  `es_sanftmut` tinyint(1) NOT NULL DEFAULT 1,
  `es_honigw` tinyint(1) NOT NULL DEFAULT 1,
  `es_brutw` tinyint(1) NOT NULL DEFAULT 1,
  `es_pollenw` tinyint(1) NOT NULL DEFAULT 1,
  `es_drohnenw` tinyint(1) NOT NULL DEFAULT 1,
  `es_leerw` tinyint(1) NOT NULL DEFAULT 1,
  `es_mittelw` tinyint(1) NOT NULL DEFAULT 1,
  `es_drohnenbn` tinyint(1) NOT NULL DEFAULT 1,
  `es_bannwaben` tinyint(1) NOT NULL DEFAULT 1,
  `es_milchsaeure` tinyint(1) NOT NULL DEFAULT 1,
  `es_waermebehandlung` tinyint(1) NOT NULL DEFAULT 1,
  `es_ablegerbildung` tinyint(1) NOT NULL DEFAULT 1,
  `es_brutwabenentsorgung` tinyint(1) NOT NULL DEFAULT 1,
  `es_phosphorsaeureester` tinyint(1) NOT NULL DEFAULT 1,
  `es_pyrethroide` tinyint(1) NOT NULL DEFAULT 1,
  `es_metarhizium-pilz` tinyint(1) NOT NULL DEFAULT 1,
  `es_buecherskorpion` tinyint(1) NOT NULL DEFAULT 1,
  `es_aetherische-oele` tinyint(1) NOT NULL DEFAULT 1,
  `es_ameisensaeure_schwammtuch` tinyint(1) NOT NULL DEFAULT 1,
  `es_ameisensaeure_universal_verdunster` tinyint(1) NOT NULL DEFAULT 1,
  `es_ameisensaeure_liebig_verdunster` tinyint(1) NOT NULL DEFAULT 1,
  `es_ameisensaeure_nassenheider_verdunster` tinyint(1) NOT NULL DEFAULT 1,
  `es_oxalsaeure_traeufeln` tinyint(1) NOT NULL DEFAULT 1,
  `es_oxalsaeure_spruehen` tinyint(1) NOT NULL DEFAULT 1,
  `es_oxalsaeure_e-verdampfer_mit_thermostat` tinyint(1) NOT NULL DEFAULT 1,
  `es_oxalsaeure_e-verdampfer_ohne_thermostat` tinyint(1) NOT NULL DEFAULT 1,
  `es_oxalsaeure_gas_verdampfer` tinyint(1) NOT NULL DEFAULT 1,
  `es_oxalsaeure_vernebler` tinyint(1) NOT NULL DEFAULT 1,
  `es_user` varchar(50) NOT NULL,
  PRIMARY KEY (`es_user`),
  CONSTRAINT `fk_es_einstellungen_s_stoecke` FOREIGN KEY (`es_user`) REFERENCES `u_user` (`u_name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `es_einstellungen`
--

LOCK TABLES `es_einstellungen` WRITE;
/*!40000 ALTER TABLE `es_einstellungen` DISABLE KEYS */;
INSERT INTO `es_einstellungen` VALUES (1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,'elaine124'),(1,1,1,1,1,1,1,1,1,1,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'elaine125');
/*!40000 ALTER TABLE `es_einstellungen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `k_koeniginnen`
--

DROP TABLE IF EXISTS `k_koeniginnen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `k_koeniginnen` (
  `k_id` int(11) NOT NULL AUTO_INCREMENT,
  `k_status` tinyint(1) NOT NULL DEFAULT 1,
  `k_name` varchar(100) NOT NULL,
  `k_jahrgang` int(11) NOT NULL,
  `k_infos` varchar(100) DEFAULT NULL,
  `k_user` varchar(50) NOT NULL,
  PRIMARY KEY (`k_id`),
  KEY `fk_k_koeniginnen_u_user1_idx` (`k_user`),
  CONSTRAINT `fk_k_koeniginnen_u_user1` FOREIGN KEY (`k_user`) REFERENCES `u_user` (`u_name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `k_koeniginnen`
--

LOCK TABLES `k_koeniginnen` WRITE;
/*!40000 ALTER TABLE `k_koeniginnen` DISABLE KEYS */;
INSERT INTO `k_koeniginnen` VALUES (3,1,'Demeter 1',2021,'','elaine124'),(8,1,'Mikado',2018,'','elaine125'),(9,1,'Athena',2019,'','elaine125'),(10,1,'Hera',2021,'','elaine125'),(11,1,'Nilofa Theresa',2018,'','elaine125'),(12,1,'Demeter 2',2017,'','elaine125'),(13,1,'Athena',2018,'','elaine124');
/*!40000 ALTER TABLE `k_koeniginnen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `s_stoecke`
--

DROP TABLE IF EXISTS `s_stoecke`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_stoecke` (
  `s_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_status` tinyint(1) NOT NULL DEFAULT 1,
  `s_name` varchar(100) NOT NULL,
  `s_infos` varchar(100) DEFAULT NULL,
  `s_koenigin` int(11) DEFAULT NULL,
  `s_standorte` int(11) DEFAULT NULL,
  PRIMARY KEY (`s_id`),
  KEY `fk_s_stoecke_st_standorte1_idx` (`s_standorte`),
  KEY `fk_s_stoecke_k_koeniginnen1_idx` (`s_koenigin`),
  CONSTRAINT `fk_s_stoecke_k_koeniginnen1` FOREIGN KEY (`s_koenigin`) REFERENCES `k_koeniginnen` (`k_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_s_stoecke_st_standorte1` FOREIGN KEY (`s_standorte`) REFERENCES `st_standorte` (`st_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `s_stoecke`
--

LOCK TABLES `s_stoecke` WRITE;
/*!40000 ALTER TABLE `s_stoecke` DISABLE KEYS */;
INSERT INTO `s_stoecke` VALUES (6,1,'Stock 1','',NULL,5),(7,1,'Stock 2','',NULL,5),(8,1,'Mein erster Stock','',NULL,6),(10,1,'3.Stock','',8,6),(13,1,'Mein zweiter Stock','',8,6),(14,1,'Stock 5','',11,6),(15,1,'Stock 1','',8,9),(16,1,'Mein zweiter Stock','',3,4),(17,1,'Stock3','',NULL,4),(18,1,'Stock 3','',3,4);
/*!40000 ALTER TABLE `s_stoecke` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `st_standorte`
--

DROP TABLE IF EXISTS `st_standorte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `st_standorte` (
  `st_id` int(11) NOT NULL AUTO_INCREMENT,
  `st_status` tinyint(1) NOT NULL DEFAULT 1,
  `st_name` varchar(100) NOT NULL,
  `st_adresse` varchar(100) DEFAULT NULL,
  `st_infos` varchar(100) DEFAULT NULL,
  `st_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`st_id`),
  KEY `fk_st_standorte_u_user1_idx` (`st_user`),
  CONSTRAINT `fk_st_standorte_u_user1` FOREIGN KEY (`st_user`) REFERENCES `u_user` (`u_name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `st_standorte`
--

LOCK TABLES `st_standorte` WRITE;
/*!40000 ALTER TABLE `st_standorte` DISABLE KEYS */;
INSERT INTO `st_standorte` VALUES (4,1,'Einsiedeleigasse 22','https://www.google.com/maps/place/Einsiedeleigasse+22,+1130+Wien/@48.1840913,16.2622128,574m/data=!3','','elaine124'),(5,1,'Einsiedeleigasse 22','https://www.google.com/maps/place/Einsiedeleigasse+22,+1130+Wien/@48.1840913,16.2622128,574m/data=!3','','elaine125'),(6,1,'Hohe Wand','','','elaine125'),(9,1,'HTL Spengergasse','','Auf dem Dach','elaine125'),(10,1,'HTL Spengergasse 3','','Auf dem Dach','elaine124');
/*!40000 ALTER TABLE `st_standorte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_token`
--

DROP TABLE IF EXISTS `t_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_token` (
  `t_id` varchar(100) NOT NULL,
  `t_email` varchar(100) NOT NULL,
  `t_datetime` datetime NOT NULL,
  PRIMARY KEY (`t_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_token`
--

LOCK TABLES `t_token` WRITE;
/*!40000 ALTER TABLE `t_token` DISABLE KEYS */;
INSERT INTO `t_token` VALUES ('-1355740139','elaine124','2022-01-20 21:51:32'),('-1431129309','elaine124','2021-11-18 14:15:50'),('-1592668091','elaine124','2022-01-20 18:57:38'),('-1766976716','elaine124','2022-01-21 09:31:38'),('-1890081748','elaine125','2022-01-20 20:57:02'),('-2045710623','elaine124','2022-01-21 08:36:38'),('-2131484182','elaine124','2022-01-21 08:34:57'),('-353272463','elaine125','2022-01-20 21:23:24'),('-415283893','elaine12','2022-01-20 20:25:05'),('-655920979','elaine124','2022-01-20 21:42:50'),('-817430822','elaine12','2022-01-20 20:55:50'),('1215771036','elaine124','2022-01-20 19:00:41'),('1596385101','elaine124','2021-11-18 14:16:45'),('1754884583','elaine12','2022-01-20 20:43:16'),('1795860981','elaine124','2022-01-21 08:36:58'),('2672139','elaine.fink19@gmail.com','2021-11-18 13:47:07');
/*!40000 ALTER TABLE `t_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `u_user`
--

DROP TABLE IF EXISTS `u_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `u_user` (
  `u_name` varchar(50) NOT NULL,
  `u_email` varchar(100) NOT NULL,
  `u_telefon` varchar(25) DEFAULT NULL,
  `u_hash` varchar(100) NOT NULL,
  `u_salt` varchar(100) NOT NULL,
  `u_bild` int(11) DEFAULT NULL,
  `u_status` enum('aktiv','nicht aktiv','Im Wartezustand') NOT NULL,
  PRIMARY KEY (`u_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `u_user`
--

LOCK TABLES `u_user` WRITE;
/*!40000 ALTER TABLE `u_user` DISABLE KEYS */;
INSERT INTO `u_user` VALUES ('elaine124','elaine.fink19@gmail.com','','386201703','0.930502803914361',14,'aktiv'),('elaine125','windrose1960 @gmail.com','','97487412','0.008878386747132483',10,'aktiv');
/*!40000 ALTER TABLE `u_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'k165810_seethebee'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-25 13:14:37
