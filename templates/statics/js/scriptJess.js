
const linkurl = "http://37.221.195.234:5050";
class Koenigin {
  constructor(k_id, k_name, k_jahrgang, k_infos, k_status, k_user) {
    this.k_id = k_id;
    this.k_name = k_name;
    this.k_jahrgang = k_jahrgang;
    this.k_infos = k_infos;
    this.k_status = k_status;
    this.k_user = k_user;
  }
};

class Standort {
    constructor(st_id, st_name, st_adresse, st_infos, st_status, st_user) {
        this.st_id = st_id;
      this.st_name = st_name;
      this.st_adresse = st_adresse;
      this.st_infos = st_infos;
      this.st_status = st_status;
        this.st_user = st_user;
    }
  };
class Stock {
    constructor(s_id, s_name, s_infos, s_koenigin, s_standorte, s_status) {
        this.s_id = s_id;
      this.s_name = s_name;
      this.s_infos = s_infos;
      this.s_koenigin = s_koenigin;
      this.s_standorte = s_standorte;
      this.s_status = s_status;
    }
  };

function loadAll() {
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let bild = urlParams.get('bild');
    let token = urlParams.get('token');
    menuezeile();
    document.getElementById('h_eintrag').href="stockkartenformular.html?name="+name+"&bild="+bild+"&id=null"+"&token="+token;
    document.getElementById('h_behandlung').href="behandlungsformular.html?name="+name+"&bild="+bild+"&id=null"+"&token="+token;

    let http_request = new XMLHttpRequest();

    http_request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log(this);
            console.log(this.responseTex);
            let kList = JSON.parse(this.responseText);
            for (let i = 0; i < kList.length; i++) {
                console.log("kList");
                let node_tr = document.createElement("tr");

                let node_td1 = document.createElement("td");
                node_tr.append(node_td1);
                let node_text1 = document.createTextNode("icon");
                node_td1.appendChild(node_text1);

                let node_td2 = document.createElement("td");
                node_tr.append(node_td2);
                let node_text2 = document.createTextNode(kList[i]["k_name"]);
                node_td2.appendChild(node_text2);

                let node_td3 = document.createElement("td");
                node_tr.append(node_td3);
                let node_text3 = document.createElement("BUTTON");
                node_text3.innerHTML = "bearbeiten";
                node_text3.setAttribute("onclick", "window.location.href='koeniginEdit.html?id=" + kList[i]["k_id"] +"&name="+name+"&bild="+bild+"&token="+token+"'");
                node_td3.appendChild(node_text3);
                let node_td4 = document.createElement("td");
                node_tr.append(node_td4);
                let node_text4 = document.createElement("BUTTON");
                node_text4.innerHTML = "löschen";
                node_text4.setAttribute("onclick", "deleteKoenigin(" + kList[i]["k_id"] + ")");
                node_td4.appendChild(node_text4);

                document.getElementById("kbody").append(node_tr);
            }
        }
    };
    let s = linkurl+"/queens/user/" + name;
    http_request.open("GET", s, true);
    http_request.send();

    let http_request2 = new XMLHttpRequest();
    http_request2.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {

            let sList = JSON.parse(this.responseText);
            for (let i = 0; i < sList.length; i++) {
                let node_trS = document.createElement("tr");

                let node_tdS1 = document.createElement("td");
                node_trS.append(node_tdS1);
                let node_textS1 = document.createTextNode("icon");
                node_tdS1.appendChild(node_textS1);

                let node_tdS2 = document.createElement("td");
                node_trS.append(node_tdS2);
                let node_textS2 = document.createTextNode(sList[i]["st_name"]);
                node_tdS2.appendChild(node_textS2);

                let node_tdS3 = document.createElement("td");
                node_trS.append(node_tdS3);
                let node_textS3 = document.createElement("BUTTON");
                node_textS3.innerHTML = "bearbeiten";
                node_textS3.setAttribute("onclick", "window.location.href='standortEdit.html?id=" + sList[i]["st_id"] +"&name="+name+"&bild="+bild+"&token="+token+"'");
                node_tdS3.appendChild(node_textS3);
                let node_tdS4 = document.createElement("td");
                node_trS.append(node_tdS4);
                let node_textS4 = document.createElement("BUTTON");
                node_textS4.innerHTML = "löschen";
                node_textS4.setAttribute("onclick", "deleteStandort(" + sList[i]["st_id"] + ")");
                node_tdS4.appendChild(node_textS4);

                let st_id = ""+sList[i]["st_id"];
                let st_name = ""+sList[i]["st_name"];

                let node_tdS5 = document.createElement("td");
                node_trS.append(node_tdS5);
                let node_textS5 = document.createElement("BUTTON");
                node_textS5.innerHTML = "Maps";

                console.log(sList[i]["st_adresse"]);
                node_textS5.setAttribute("onclick", "window.location.href ='map.html?name="+name+"&bild="+bild+"&st_id="+st_id+"&st_name="+st_name+"&token="+token+"'");
                node_tdS5.appendChild(node_textS5);
                let node_tdS6 = document.createElement("td");
                node_trS.append(node_tdS6);
                let node_textS6 = document.createElement("BUTTON");
                node_textS6.innerHTML = "Stöcke";

                node_textS6.setAttribute("onclick", "window.location.href ='stock.html?name="+name+"&bild="+bild+"&st_id="+st_id+"&st_name="+st_name+"&token="+token+"'");
                node_tdS6.appendChild(node_textS6);

                document.getElementById("sbody").append(node_trS);
            }
        }
    };

    let s2 = linkurl+"/orte/user/" + name;
    http_request2.open("GET", s2 , true);
    http_request2.send();
}

function loadStoecke() {
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let bild = urlParams.get('bild');
    let token = urlParams.get('token');
    let st_id = ""+urlParams.get('st_id');
    let st_name = ""+ urlParams.get('st_name');

    console.log(name + " - " + bild+ " - " + st_id);
    menuezeile();
    let http_request = new XMLHttpRequest();
    http_request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let stList = JSON.parse(this.responseText);
                let node_trh = document.createElement("tr");
                let node_th = document.createElement("th");

                //getName(st_id);
                let node_text = document.createTextNode("Stöcke von "+st_name);
                node_th.appendChild(node_text);
                node_trh.append(node_th);

                let node_tdh = document.createElement("td");
                node_trh.append(node_tdh);
                let node_textdh = document.createElement("BUTTON");
                node_textdh.innerHTML = "hinzufügen";
                node_textdh.setAttribute("onclick", "loadStockadd()");
                node_tdh.appendChild(node_textdh);


                let node_tdh2 = document.createElement("td");
                node_trh.append(node_tdh2);
                let node_textdh2 = document.createElement("BUTTON");
                node_textdh2.innerHTML = "zurück";
                node_textdh2.setAttribute("onclick", "loadHome()");
                node_tdh2.appendChild(node_textdh2);

                document.getElementById("sthead").append(node_trh);

            for (let i = 0; i < stList.length; i++) {

                let node_tr = document.createElement("tr");

                let node_td1 = document.createElement("td");
                node_tr.append(node_td1);
                let node_text1 = document.createTextNode("icon");
                node_td1.appendChild(node_text1);

                let node_td2 = document.createElement("td");
                node_tr.append(node_td2);
                let node_text2 = document.createTextNode(stList[i]["s_name"]);
                node_td2.appendChild(node_text2);

                let s_id = ""+stList[i]["s_id"];
                let s_name = ""+stList[i]["s_name"];

                let node_td = document.createElement("td");
                node_tr.append(node_td);
                let node_text = document.createElement("BUTTON");
                node_text.innerHTML = "Einträge";
                node_text.setAttribute("onclick", "window.location.href ='stockEintraege.html?name="+name+"&bild="+bild+"&s_id="+s_id+"&s_name="+s_name+"&st_id="+st_id+"&st_name="+st_name+"&token="+token+"'");
                node_td.appendChild(node_text);

                let node_td3 = document.createElement("td");
                node_tr.append(node_td3);
                let node_text3 = document.createElement("BUTTON");
                node_text3.innerHTML = "bearbeiten";
                node_text3.setAttribute("onclick", "window.location.href='stockEdit.html?st_id=" + st_id +"&st_name="+st_name+"&name="+name+"&bild="+bild+"&s_id="+ s_id+"&token="+token+"'");
                node_td3.appendChild(node_text3);

                let node_td4 = document.createElement("td");
                node_tr.append(node_td4);
                let node_text4 = document.createElement("BUTTON");
                node_text4.innerHTML = "löschen";
                node_text4.setAttribute("onclick", "deleteStock(" + stList[i]["s_id"] + ")");
                node_td4.appendChild(node_text4);

                document.getElementById("stbody").append(node_tr);
                console.log("loadStoecke(): "+s_id);
            }
        }
    };
    let s = linkurl+"/stoecke/ort/" + st_id;
    http_request.open("GET", s, true);
    http_request.send();
}
function loadSEintraege() {
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let bild = urlParams.get('bild');
    let token = urlParams.get('token');
    let s_id = ""+urlParams.get('s_id');
    let s_name = ""+ urlParams.get('s_name');
    let st_id = ""+urlParams.get('st_id');
    let st_name = ""+ urlParams.get('st_name');
    document.getElementById('profilimg').src= "../statics/img/"+bild+".png";
    document.getElementById('einst').href="einstellungen.html?name="+name+"&bild="+bild + "&token="+token;

    document.getElementById('h_eintrag').href="stockkartenformular.html?name="+name+"&bild="+bild+"&id=null"+"&token="+token;
    document.getElementById('h_behandlung').href="behandlungsformular.html?name="+name+"&bild="+bild+"&id=null"+"&token="+token;
    document.getElementById('home').href="home.html?name="+name+"&bild="+bild+"&token="+token;
    menuezeile()

    let node_trh = document.createElement("tr");
            let node_th = document.createElement("th");

            let node_text = document.createTextNode("Stockeinträge von " + s_name);
            node_th.appendChild(node_text);
            node_trh.append(node_th);

            let node_tdh2 = document.createElement("td");
            node_trh.append(node_tdh2);
            let node_textdh2 = document.createElement("BUTTON");
            node_textdh2.innerHTML = "zurück";
            node_textdh2.setAttribute("onclick", "loadStock("+st_id+")");
            node_tdh2.appendChild(node_textdh2);

            document.getElementById("bhead").append(node_trh);

            //Stockkarte
            let node_trh3 = document.createElement("tr");
            let node_th3 = document.createElement("th");

            let node_text3 = document.createTextNode("Stockkarten");
            node_th3.appendChild(node_text3);
            node_trh3.append(node_th3);
            document.getElementById("bhead").append(node_trh3);
            //Behandlung
            let node_trh4 = document.createElement("tr");
            let node_th4 = document.createElement("th");
            let node_text4 = document.createTextNode("Behandlungen");
            node_th4.appendChild(node_text4);
            node_trh4.append(node_th4);
            document.getElementById("bbhead").append(node_trh4);


            let http_request = new XMLHttpRequest();
            http_request.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    let eListe = JSON.parse(this.responseText);
                                        console.log("angekommen");
            for (let i = 0; i < eListe.length; i++) {
                console.log("eListe");
                let node_tr = document.createElement("tr");

                let node_td1 = document.createElement("td");
                node_tr.append(node_td1);
                let node_text1 = document.createTextNode("icon");
                node_td1.appendChild(node_text1);
                let node_td2 = document.createElement("td");
                node_tr.append(node_td2);
                let node_text2 = document.createTextNode("Eintrag am "+eListe[i]["e_date"]);
                node_td2.appendChild(node_text2);

                let node_td3 = document.createElement("td");
                node_tr.append(node_td3);
                let node_text3 = document.createElement("BUTTON");
                node_text3.innerHTML = "bearbeiten";
                node_text3.setAttribute("onclick", "window.location.href='stockkartenformular.html?id=" + eListe[i]["e_id"] +"&name="+name+"&bild="+bild+"&token="+token+"'");
                node_td3.appendChild(node_text3);
                let node_td4 = document.createElement("td");
                node_tr.append(node_td4);
                let node_text4 = document.createElement("BUTTON");
                node_text4.innerHTML = "löschen";
                node_text4.setAttribute("onclick", "deleteEintrag(" + eListe[i]["e_id"] + ")");
                node_td4.appendChild(node_text4);

                document.getElementById("ebody").append(node_tr);
            }
        }
    };
    let s = linkurl+"/entrys/stock/" + s_id;
    http_request.open("GET", s, true);
    http_request.send();

    let http_request2 = new XMLHttpRequest();
    http_request2.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {

            let bList = JSON.parse(this.responseText);
            for (let i = 0; i < bList.length; i++) {
                let node_trS = document.createElement("tr");

                let node_tdS1 = document.createElement("td");
                node_trS.append(node_tdS1);
                let node_textS1 = document.createTextNode("icon");
                node_tdS1.appendChild(node_textS1);

                let node_tdS2 = document.createElement("td");
                node_trS.append(node_tdS2);
                let node_textS2 = document.createTextNode("Behandlung am " + bList[i]["b_datevon"]);
                node_tdS2.appendChild(node_textS2);

                let node_tdS3 = document.createElement("td");
                node_trS.append(node_tdS3);
                let node_textS3 = document.createElement("BUTTON");
                node_textS3.innerHTML = "bearbeiten";
                node_textS3.setAttribute("onclick", "window.location.href='behandlungsformular.html?id=" + bList[i]["b_id"] +"&name="+name+"&bild="+bild+"&token="+token+"'");
                node_tdS3.appendChild(node_textS3);
                let node_tdS4 = document.createElement("td");
                node_trS.append(node_tdS4);
                let node_textS4 = document.createElement("BUTTON");
                node_textS4.innerHTML = "löschen";
                node_textS4.setAttribute("onclick", "deleteBehandlung(" + bList[i]["b_id"] + ")");
                node_tdS4.appendChild(node_textS4);

                document.getElementById("bbody").append(node_trS);
            }
        }
    };
    let s2 = linkurl+"/treatments/stock/" + s_id;
    http_request2.open("GET", s2 , true);
    http_request2.send();
}
function loadKoenigin(){
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let bild = urlParams.get('bild');
    let token = urlParams.get('token');
    window.location.href = "koenigin.html?name="+name+"&bild="+bild+"&token="+token;
}
function loadHome(){
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let bild = urlParams.get('bild');
    let token = urlParams.get('token');
    window.location.href = "home.html?name="+name+"&bild="+bild+"&token="+token;
}
function loadStandort(){
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let bild = urlParams.get('bild');
    let token = urlParams.get('token');
    window.location.href = "standort.html?name="+name+"&bild="+bild+"&token="+token;

}

function loadStock(st_id){
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let bild = urlParams.get('bild');
    let st_name = urlParams.get('st_name');
    let token = urlParams.get('token');
    window.location.href = "stock.html?name="+name+"&bild="+bild+"&st_id="+st_id+"&st_name="+st_name+"&token="+token;
}
function loadStockadd(){
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let bild = urlParams.get('bild');
    let st_id = urlParams.get('st_id');
    let st_name = urlParams.get('st_name');
    let token = urlParams.get('token');
    window.location.href = "stockadd.html?name="+name+ "&st_id="+st_id+ "&st_name="+st_name+"&bild="+bild+"&token="+token;
}
function loadEintraege(){
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let bild = urlParams.get('bild');
    let token = urlParams.get('token');
    let s_id = urlParams.get('s_id');
    let s_name = urlParams.get('s_name');
    let st_id = urlParams.get('st_id');
    let st_name = urlParams.get('st_name');
    window.location.href ="stockEintraege.html?name="+name+"&bild="+bild+"&s_id="+s_id+"&s_name="+s_name+"&st_id="+st_id+"&st_name="+st_name+"&token="+token;
}
function loadStatistik(){
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let bild = urlParams.get('bild');
    let token = urlParams.get('token');
    menuezeile();
    window.location.href = "statistik.html?name="+name+"&bild="+bild+""+"&token="+token;
}

function reloadAll(){
    var kbody = document.getElementById("kbody");
    var sbody = document.getElementById("sbody");
    for(var i = kbody.rows.length - 1; i > -1; i--)
    {
        kbody.deleteRow(i);
    }
    for(var i = sbody.rows.length - 1; i > -1; i--)
    {
        sbody.deleteRow(i);
    }
    loadHome();
}

function addKoenigin(){
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let k = new Koenigin(
        0,
        document.getElementById("inputName").value,
        document.getElementById("inputJahrgang").value,
        document.getElementById("inputInfos").value,
        1,
        name
    );
    console.log(JSON.stringify(k));
    console.log(name);
    let http_request = new XMLHttpRequest();
    let a = linkurl+"/queen/" +name+"";
    http_request.open("POST", a, true);
    http_request.send(JSON.stringify(k));
    loadHome();
}
function addStandort(){
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let http_request = new XMLHttpRequest();
    let standort = new Standort(
        0,
        document.getElementById("inputName").value,
        document.getElementById("inputAdresse").value,
        document.getElementById("inputInfos").value,
        1,
        name
    );
    let a = linkurl+"/ort/" +name+"";
    http_request.open("POST", a, true);
    http_request.send(JSON.stringify(standort));
    loadHome();
}
function addStock(){
    let urlParams = new URLSearchParams(window.location.search);
    let st_id = urlParams.get('st_id');
    let name = urlParams.get('name');
    let stock = new Stock(
        0,
        document.getElementById("inputName").value,
        document.getElementById("inputInfos").value,
        document.getElementById("inputKoenigin").value,
        st_id,
        1
    );
    console.log(JSON.stringify(stock));

    let http_request = new XMLHttpRequest();
    let a = linkurl+"/stock/" +st_id+"";
    http_request.open("POST", a, true);
    http_request.send(JSON.stringify(stock));
    loadStock(st_id);
}
function deleteKoenigin(id){
    let http_request = new XMLHttpRequest();
    let a = linkurl+"/queen/" +id+"";
    http_request.open("DELETE", a, true);
    http_request.send();
    reloadAll();
}
function deleteStandort(id){
    let http_request = new XMLHttpRequest();
    let a = linkurl+"/ort/" +id+"";
    http_request.open("DELETE", a, true);
    http_request.send();
    reloadAll();
}
function deleteStock(id) {
    let urlParams = new URLSearchParams(window.location.search);
    let st_id = urlParams.get('st_id');
    let http_request = new XMLHttpRequest();
    let a = linkurl+"/stock/" + id + "";
    http_request.open("DELETE", a, true);
    http_request.send();
    loadStock(st_id);
}
function deleteEintrag(id){
    let http_request = new XMLHttpRequest();
    let a = linkurl+"/entry/" +id+"";
    http_request.open("DELETE", a, true);
    http_request.send();
    loadEintraege();
}
function deleteBehandlung(id){
    let http_request = new XMLHttpRequest();
    let a = linkurl+"/treatment/" +id+"";
    http_request.open("DELETE", a, true);
    http_request.send();
    loadEintraege();
}
function editKoenigin(){
    let urlParams = new URLSearchParams(window.location.search);
    let id = urlParams.get('id');
    let name = urlParams.get('name');
    let koenigin = new Koenigin(
        parseInt(id),
        document.getElementById("inputName").value,
        document.getElementById("inputJahrgang").value,
        document.getElementById("inputInfos").value,
        1,
        name
    );
    let http_request = new XMLHttpRequest();
    http_request.open("PUT", linkurl+"/queen/" + id, true);
    http_request.send(JSON.stringify(koenigin));
    loadHome();
}
function editStandort(){
    let urlParams = new URLSearchParams(window.location.search);
    let id = urlParams.get('id');
    let name = urlParams.get('name');
    let standort = new Standort(
        parseInt(id),
        document.getElementById("inputName").value,
        document.getElementById("inputAdresse").value,
        document.getElementById("inputInfos").value,
        1,
        name
    );
    let http_request = new XMLHttpRequest();
    http_request.open("PUT", linkurl+"/ort/" + id, true);
    http_request.send(JSON.stringify(standort));
    loadHome();
}
function editStock(){
    let urlParams = new URLSearchParams(window.location.search);
    let st_id = ""+urlParams.get('st_id');
    let s_id = urlParams.get('s_id');
    let name = urlParams.get('name');
    let stock = new Stock(
        parseInt(s_id),
        document.getElementById("inputName").value,
        document.getElementById("inputInfos").value,
        document.getElementById("inputKoenigin").value,
        st_id,
        1
    );
    let http_request = new XMLHttpRequest();
    http_request.open("PUT", linkurl+"/stock/" + s_id, true);
    http_request.send(JSON.stringify(stock));
    loadStock(st_id);
}
function loadK() {
    let urlParams = new URLSearchParams(window.location.search);
    let id = urlParams.get('id');
    loadKoeniginById(id);
    menuezeile();
}
function loadSt() {
    let urlParams = new URLSearchParams(window.location.search);
    let id = urlParams.get('id');
    menuezeile();
    loadStandortById(id);
}
function loadS(){
    let urlParams = new URLSearchParams(window.location.search);
    let s_id = urlParams.get('s_id');
    console.log("loadS(): "+s_id);
    loadStockById(s_id);
    menuezeile();
}
//Königin bearbeiten einsetzen
function loadKoeniginById(id) {
    let http_request = new XMLHttpRequest();
    http_request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let koenigin = JSON.parse(this.responseText);
            document.getElementById("inputName").value = koenigin["k_name"];
            document.getElementById("inputJahrgang").value = koenigin["k_jahrgang"];
            document.getElementById("inputInfos").value = koenigin["k_infos"];
        }
    };
    http_request.open("GET", linkurl+"/queen/" + id, true);
    http_request.send();
}
function loadStandortById(id) {
    let http_request = new XMLHttpRequest();
    http_request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let standort = JSON.parse(this.responseText);
            document.getElementById("inputName").value = standort["st_name"];
            document.getElementById("inputAdresse").value = standort["st_adresse"];
            document.getElementById("inputInfos").value = standort["st_infos"];
        }
    };
    http_request.open("GET", linkurl+"/ort/" + id, true);
    http_request.send();
}
function loadStockById(id) {
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let s_id = urlParams.get('s_id');
    let st_id = urlParams.get('st_id');
    let s_name = urlParams.get('s_name');
    menuezeile();
    let node_trh = document.createElement("tr");
                let node_tdh2 = document.createElement("td");
                node_trh.append(node_tdh2);
                let node_textdh2 = document.createElement("BUTTON");
                node_textdh2.innerHTML = "zurück";
                node_textdh2.setAttribute("onclick", "loadStock("+st_id+")");
                node_tdh2.appendChild(node_textdh2);

                let node_tdh3 = document.createElement("td");
                node_trh.append(node_tdh3);
                let node_textdh3 = document.createElement("BUTTON");
                node_textdh3.innerHTML = "speichern";
                node_textdh3.setAttribute("onclick", "editStock()");
                node_tdh3.appendChild(node_textdh3);

                document.getElementById("stockdiv2").append(node_trh);

    let http_request = new XMLHttpRequest();
    http_request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let stock = JSON.parse(this.responseText);
            document.getElementById("inputName").value = stock["s_name"];
            document.getElementById("inputInfos").value = stock["s_infos"];
            loadStockKoenigin2();
            console.log("loadStockById(): "+id);
        }
    };
    http_request.open("GET", linkurl+"/stock/" + id, true);
    http_request.send();
}

var koeniginName = [];

//add stock einsetzen
function loadStockKoenigin(){
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let st_id = urlParams.get('st_id');

    menuezeile();

    let node_trh = document.createElement("tr");
            let node_tdh2 = document.createElement("td");
            node_trh.append(node_tdh2);
            let node_textdh2 = document.createElement("BUTTON");
            node_textdh2.innerHTML = "zurück";
            node_textdh2.setAttribute("onclick", "loadStock("+st_id+")");
            node_tdh2.appendChild(node_textdh2);

            let node_tdh3 = document.createElement("td");
            node_trh.append(node_tdh3);
            let node_textdh3 = document.createElement("BUTTON");
            node_textdh3.innerHTML = "speichern";
            node_textdh3.setAttribute("onclick", "addStock()");
            node_tdh3.appendChild(node_textdh3);

            document.getElementById("stockdiv").append(node_trh);

    let http_request = new XMLHttpRequest();
    http_request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log("elo");
            let kList = JSON.parse(this.responseText);
            for (let i = 0; i < kList.length; i++) {
                koeniginName.push(this.responseText);
                const newOption = document.createElement('option');
                const optionText = document.createTextNode(''+kList[i]["k_name"]);
                // set option text
                newOption.appendChild(optionText);
                // and option value
                newOption.setAttribute('value',kList[i]["k_id"]);
                // add the option to the select box
                document.getElementById("inputKoenigin").appendChild(newOption);
            }
        }
    };
    let s = linkurl+"/queens/user/" + name;
    http_request.open("GET", s, true);
    http_request.send();

    //console.log(JSON.stringify(koeniginID));
    console.log(JSON.stringify(koeniginName));
}
//edit stock einsetzen
function loadStockKoenigin2(){
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let st_id = urlParams.get('st_id');

    let http_request = new XMLHttpRequest();
    http_request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log("elo2");
            let kList = JSON.parse(this.responseText);
            for (let i = 0; i < kList.length; i++) {
                koeniginName.push(this.responseText);
                const newOption = document.createElement('option');
                const optionText = document.createTextNode(''+kList[i]["k_name"]);
                // set option text
                newOption.appendChild(optionText);
                // and option value
                newOption.setAttribute('value',kList[i]["k_id"]);
                // add the option to the select box
                document.getElementById("inputKoenigin").appendChild(newOption);
            }
        }
    }
    let s = linkurl+"/queens/user/" + name;
    http_request.open("GET", s, true);
    http_request.send();

    //console.log(JSON.stringify(koeniginID));
    console.log(JSON.stringify(koeniginName));
}
function selectElement(id, valueToSelect) {
    let element = document.getElementById(id);
    element.value = valueToSelect;
}
function loadMaps(adresse){
    /*console.log(adresse);
    window.open(adresse);*/
    window.location.href="map.html";
}
function loadSteetmaps(){
    var mymap = L.map('mapid').setView([48.2083, 16.3731], 13);
}

var labels = [];
var honigw = [];
var pollenw = [];
var brutw = [];
var mittel = [];
var leer = [];
var drohnen = [];
var varroa =[];
var weisel = [];
var propolis = [];
var sanftmut = [];
var stetigkeit = [];
var wirrbau = [];

function StatistikWerte(s_id){
    labels = [];
 honigw = [];
 pollenw = [];
 brutw = [];
 mittel = [];
 leer = [];
 drohnen = [];
 varroa =[];
 weisel = [];
 propolis = [];
 sanftmut = [];
 stetigkeit = [];
 wirrbau = [];

    document.getElementById("myChart").innerHTML="";
    console.log("Stock:" +s_id);
        let http_request = new XMLHttpRequest();
            http_request.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    let eListe = JSON.parse(this.responseText);
                    console.log("list")
                    for (let i = 0; i < eListe.length; i++) {
                        if(labels.includes(eListe[i]["e_date"])==false){
                            labels.push(eListe[i]["e_date"]);
                        }
                        honigw.push(eListe[i]["e_honigw"]);
                        pollenw.push(eListe[i]["e_pollenw"]);
                        brutw.push(eListe[i]["e_brutw"]);
                        mittel.push(eListe[i]["e_mittelw"]);
                        leer.push(eListe[i]["e_leerw"]);
                        drohnen.push(eListe[i]["e_drohnenw"]);
                        varroa.push(eListe[i]["e_varroa"]);
                        weisel.push(eListe[i]["e_weiselz"]);
                        sanftmut.push(eListe[i]["e_sanftmut"]);
                        propolis.push(eListe[i]["e_propolis"]);
                        stetigkeit.push(eListe[i]["e_stetigkeit"]);
                        wirrbau.push(eListe[i]["e_wirrbau"]);
                         }
                StatistikBehandlung(s_id);
        }
    };
    let s = linkurl+"/entrys/stock/" + s_id;
    http_request.open("GET", s, true);
    http_request.send();
}
function StatistikGrafik(){
     console.log("Chart");
    var ctx = document.getElementById("myChart");
    const myChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: labels,
        datasets: [
            {
                type: "line",
            data: honigw,
            label: "Honigwaben",
            borderColor: "#ffdc73",
            fill: false
          }, {
            type: "line",
            data: pollenw,
            label: "Pollenwaben",
            borderColor: "#99aab5",
            fill: false
          }, {
            type: "line",
            data: brutw,
            label: "Brutwaben",
            borderColor: "#bf9b30",
            fill: false
          }, {
            type: "line",
            data: mittel,
            label: "Mittelwände",
            borderColor: "#e8c3b9",
            fill: false
          }, {
            type: "line",
            data: leer,
            label: "Leerwaben",
            borderColor: "#8b5a2b",
            fill: false
          }, {
            type: "line",
            data: drohnen,
            label: "Drohnenwaben",
            borderColor: "#b7e0dc",
            fill: false
          }, {
            type: "line",
            data: varroa,
            label: "Varroamilben",
            borderColor: "#E9EDC9",
            fill: false
          }, {
            type: "line",
            data: weisel,
            label: "Weiselzellen",
            borderColor: "#b4c468",
            fill: false
          }, {
            type: "line",
            data: propolis,
            label: "Propolis",
            borderColor: "#dc6900",
            fill: false
          }, {
            type: "line",
            data: sanftmut,
            label: "Sanftmut",
            borderColor: "#ff9a00",
            fill: false
          }, {
            type: "line",
            data: stetigkeit,
            label: "Stetigkeit",
            borderColor: "#604439",
            fill: false
          }, {
            type: "line",
            data: wirrbau,
            label: "Wirrbau",
            borderColor: "#71c7ec",
            fill: false
          }
        ]
      },
      options: {
          spanGaps: true,
          title:{
              display: true,
              text:'Statisitk der Einträge'
          },
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true
                  }
              }],
              xAxes: [{
                  ticks: {
                      beginAtZero: true
                  }
              }]
          }
          },
    });
    StatistikBehGrafik(myChart);
    myChart.update();
}
function StatistikStandorte(){
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let http_request2 = new XMLHttpRequest();
    menuezeile();
    http_request2.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            //default option
             const newOption = document.createElement('option');
             const optionText = document.createTextNode('auswählen');
             newOption.appendChild(optionText);
             newOption.setAttribute('value', 'auswählen');
             document.getElementById("inputStandort").appendChild(newOption);
             document.getElementById('inputStandort').getElementsByTagName('option')[0].selected = 'selected';
             document.getElementById('inputStandort').getElementsByTagName('option')[0].disabled = 'disabled';


            let sList = JSON.parse(this.responseText);
            for (let i = 0; i < sList.length; i++) {
                const newOption = document.createElement('option');
                const optionText = document.createTextNode(''+sList[i]["st_name"]);
                newOption.appendChild(optionText);
                newOption.setAttribute('value',sList[i]["st_id"]);
                document.getElementById("inputStandort").appendChild(newOption);
            }
        }
    };
    let s2 = linkurl+"/orte/user/" + name;
    http_request2.open("GET", s2 , true);
    http_request2.send();
}
function StatistikStoecke(st_id){
    document.getElementById("inputStock").innerHTML = "";
    let http_request = new XMLHttpRequest();
    http_request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            //default option
             const newOption = document.createElement('option');
             const optionText = document.createTextNode('auswählen');
             newOption.appendChild(optionText);
             newOption.setAttribute('value', 'auswählen');
             document.getElementById("inputStock").appendChild(newOption);
             document.getElementById('inputStock').getElementsByTagName('option')[0].selected = 'selected';
             document.getElementById('inputStock').getElementsByTagName('option')[0].disabled = 'disabled';

            let stList = JSON.parse(this.responseText);
            for (let i = 0; i < stList.length; i++) {
                const newOption = document.createElement('option');
                const optionText = document.createTextNode(''+stList[i]["s_name"]);
                newOption.appendChild(optionText);
                newOption.setAttribute('value',stList[i]["s_id"]);
                document.getElementById("inputStock").appendChild(newOption);
            }
        }
    };
    let s = linkurl+"/stoecke/ort/" + st_id;
    http_request.open("GET", s, true);
    http_request.send();
}

var bnames = []
var bdatevons = []
var bdatebiss = []
var bdata = []
var newDataSet;

var b_waerme = []
var b_drohnenbrut = []
var b_ablegerbildung = []
var b_bannwaben = []
var b_entsorgungBrut = []
var b_phosphor = []
var b_pyrethroide = []
var b_metarhizium = []
var b_buecher = []
var b_milchsaeure = []
var b_aetherischoele = []
var b_asSchwamm = []
var b_asUniversal = []
var b_asLiebigv = []
var b_asNassenv = []
var b_oTraeufeln = []
var b_oSpruehen = []
var b_oEVerdMitT = []
var b_oEVerdOhneT = []
var b_oGasverdampfer = []
var b_oVernebler = []
var b_natuerlicheE = []


function StatistikBehandlung(s_id){
    bnames = []
    bdatevons = []
    bdatebiss = []
    bdata = []

    b_waerme = []
    b_drohnenbrut = []
    b_ablegerbildung = []
    b_bannwaben = []
    b_entsorgungBrut = []
    b_phosphor = []
    b_pyrethroide = []
    b_metarhizium = []
    b_buecher = []
    b_milchsaeure = []
    b_aetherischoele = []
    b_asSchwamm = []
    b_asUniversal = []
    b_asLiebigv = []
    b_asNassenv = []
    b_oTraeufeln = []
    b_oSpruehen = []
    b_oEVerdMitT = []
    b_oEVerdOhneT = []
    b_oGasverdampfer = []
    b_oVernebler = []
    b_natuerlicheE = []

        let http_request = new XMLHttpRequest();
            http_request.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    let eListe = JSON.parse(this.responseText);
                        for (let i = 0; i < eListe.length; i++) {
                            bnames.push(eListe[i]["b_behandlung"])
                            bdatevons.push(eListe[i]["b_datevon"])
                            bdatebiss.push(eListe[i]["b_datebis"])
                            if(eListe[i]["b_behandlung"] == "Wärmebehandlung"){
                                b_waerme.push(eListe[i]["b_datevon"])
                                if(eListe[i]["b_datebis"] != null) {
                                    dateRange(eListe[i]["b_datevon"], eListe[i]["b_datebis"], steps = 1, b_waerme, bdatevons)
                                }
                            }
                            if(eListe[i]["b_behandlung"] == "Entnahme von Drohnenbrut"){
                                b_drohnenbrut.push(eListe[i]["b_datevon"])
                                if(eListe[i]["b_datebis"] != null) {
                                    dateRange(eListe[i]["b_datevon"], eListe[i]["b_datebis"], steps = 1, b_drohnenbrut, bdatevons)
                                }
                            }
                            if(eListe[i]["b_behandlung"] == "Bannwabenverfahren"){
                                b_bannwaben.push(eListe[i]["b_datevon"])
                                if(eListe[i]["b_datebis"] != null) {
                                    dateRange(eListe[i]["b_datevon"], eListe[i]["b_datebis"], steps = 1, b_bannwaben, bdatevons)
                                }
                            }
                            if(eListe[i]["b_behandlung"] == "Ablegerbildung"){
                                b_ablegerbildung.push(eListe[i]["b_datevon"])
                                if(eListe[i]["b_datebis"] != null) {
                                    dateRange(eListe[i]["b_datevon"], eListe[i]["b_datebis"], steps = 1, b_ablegerbildung, bdatevons)
                                }
                            }
                            if(eListe[i]["b_behandlung"] == "Entsorgen der Brutwaben"){
                                b_entsorgungBrut.push(eListe[i]["b_datevon"])
                                if(eListe[i]["b_datebis"] != null) {
                                    dateRange(eListe[i]["b_datevon"], eListe[i]["b_datebis"], steps = 1, b_entsorgungBrut, bdatevons)
                                }
                            }
                            if(eListe[i]["b_behandlung"] == "organische Phosphorsäureester"){
                                b_phosphor.push(eListe[i]["b_datevon"])
                                if(eListe[i]["b_datebis"] != null) {
                                    dateRange(eListe[i]["b_datevon"], eListe[i]["b_datebis"], steps = 1, b_phosphor, bdatevons)
                                }
                            }
                            if(eListe[i]["b_behandlung"] == "Pyrethroide"){
                                b_pyrethroide.push(eListe[i]["b_datevon"])
                                if(eListe[i]["b_datebis"] != null) {
                                    dateRange(eListe[i]["b_datevon"], eListe[i]["b_datebis"], steps = 1, b_pyrethroide, bdatevons)
                                }
                            }
                            if(eListe[i]["b_behandlung"] == "Metarhizium-Pilz"){
                                b_metarhizium.push(eListe[i]["b_datevon"])
                                if(eListe[i]["b_datebis"] != null) {
                                    dateRange(eListe[i]["b_datevon"], eListe[i]["b_datebis"], steps = 1, b_metarhizium, bdatevons)
                                }
                            }
                            if(eListe[i]["b_behandlung"] == "Bücherskorpion"){
                                b_buecher.push(eListe[i]["b_datevon"])
                                if(eListe[i]["b_datebis"] != null) {
                                    dateRange(eListe[i]["b_datevon"], eListe[i]["b_datebis"], steps = 1, b_buecher, bdatevons)
                                }
                            }
                            if(eListe[i]["b_behandlung"] == "Milchsäure sprühen"){
                                b_milchsaeure.push(eListe[i]["b_datevon"])
                                if(eListe[i]["b_datebis"] != null) {
                                    dateRange(eListe[i]["b_datevon"], eListe[i]["b_datebis"], steps = 1, b_milchsaeure, bdatevons)
                                }
                            }
                            if(eListe[i]["b_behandlung"] == "ätherische Öle"){
                                b_aetherischoele.push(eListe[i]["b_datevon"])
                                if(eListe[i]["b_datebis"] != null) {
                                    dateRange(eListe[i]["b_datevon"], eListe[i]["b_datebis"], steps = 1, b_aetherischoele, bdatevons)
                                }
                            }
                            if(eListe[i]["b_behandlung"] == "Ameisensäure Schwammtuch"){
                                b_asSchwamm.push(eListe[i]["b_datevon"])
                                if(eListe[i]["b_datebis"] != null) {
                                    dateRange(eListe[i]["b_datevon"], eListe[i]["b_datebis"], steps = 1, b_asSchwamm, bdatevons)
                                }
                            }
                            if(eListe[i]["b_behandlung"] == "Ameisensäure Universal-Verdunster"){
                                b_asUniversal.push(eListe[i]["b_datevon"])
                                if(eListe[i]["b_datebis"] != null) {
                                    dateRange(eListe[i]["b_datevon"], eListe[i]["b_datebis"], steps = 1, b_asUniversal, bdatevons)
                                }
                            }
                            if(eListe[i]["b_behandlung"] == "Ameisensäure Liebig-Verdunster"){
                                b_asLiebigv.push(eListe[i]["b_datevon"])
                                if(eListe[i]["b_datebis"] != null) {
                                    dateRange(eListe[i]["b_datevon"], eListe[i]["b_datebis"], steps = 1, b_asLiebigv, bdatevons)
                                }
                            }
                            if(eListe[i]["b_behandlung"] == "Ameisensäure Nassenheider-Verdunster"){
                                b_asNassenv.push(eListe[i]["b_datevon"])
                                if(eListe[i]["b_datebis"] != null) {
                                    dateRange(eListe[i]["b_datevon"], eListe[i]["b_datebis"], steps = 1, b_asNassenv, bdatevons)
                                }
                            }
                            if(eListe[i]["b_behandlung"] == "Oxalsäure träufeln"){
                                b_oTraeufeln.push(eListe[i]["b_datevon"])
                                if(eListe[i]["b_datebis"] != null) {
                                    dateRange(eListe[i]["b_datevon"], eListe[i]["b_datebis"], steps = 1, b_oTraeufeln, bdatevons)
                                }
                            }
                            if(eListe[i]["b_behandlung"] == "Oxalsäure sprühen"){
                                b_oSpruehen.push(eListe[i]["b_datevon"])
                                if(eListe[i]["b_datebis"] != null) {
                                    dateRange(eListe[i]["b_datevon"], eListe[i]["b_datebis"], steps = 1, b_oSpruehen, bdatevons)
                                }
                            }
                            if(eListe[i]["b_behandlung"] == "Oxalsäure E-Verdampfer mit Thermostat"){
                                b_oEVerdMitT.push(eListe[i]["b_datevon"])
                                if(eListe[i]["b_datebis"] != null) {
                                    dateRange(eListe[i]["b_datevon"], eListe[i]["b_datebis"], steps = 1, b_oEVerdMitT, bdatevons)
                                }
                            }
                            if(eListe[i]["b_behandlung"] == "Oxalsäure E-Verdampfer ohne Thermostat"){
                                b_oEVerdOhneT.push(eListe[i]["b_datevon"])
                                if(eListe[i]["b_datebis"] != null) {
                                    dateRange(eListe[i]["b_datevon"], eListe[i]["b_datebis"], steps = 1, b_oEVerdOhneT, bdatevons)
                                }
                            }
                            if(eListe[i]["b_behandlung"] == "Oxalsäure Gas-Verdampfer"){
                                b_oGasverdampfer.push(eListe[i]["b_datevon"])
                                if(eListe[i]["b_datebis"] != null) {
                                    dateRange(eListe[i]["b_datevon"], eListe[i]["b_datebis"], steps = 1, b_oGasverdampfer, bdatevons)
                                }
                            }
                            if(eListe[i]["b_behandlung"] == "Oxalsäure Vernebler"){
                                b_oVernebler.push(eListe[i]["b_datevon"])
                                if(eListe[i]["b_datebis"] != null) {
                                    dateRange(eListe[i]["b_datevon"], eListe[i]["b_datebis"], steps = 1, b_oVernebler, bdatevons)
                                }
                            }
                            if(eListe[i]["b_behandlung"] == "natürliche Evolution (ohne Fremdeinwirkung)"){
                                b_natuerlicheE.push(eListe[i]["b_datevon"])
                                if(eListe[i]["b_datebis"] != null) {
                                    dateRange(eListe[i]["b_datevon"], eListe[i]["b_datebis"], steps = 1, b_natuerlicheE, bdatevons)
                                }
                            }
                        }
                StatistikGrafik();
        }
    };
    let s = linkurl+"/treatments/stock/" + s_id;
    http_request.open("GET", s, true);
    http_request.send();
}

function dateRange(startDate, endDate, steps = 1, blist, list2) {
  let currentDate = new Date(startDate)
  formatDate(currentDate)
      let arr =[]

  while (currentDate <= new Date(endDate)) {
      let newDate = new Date(currentDate)
      if(arr.includes(newDate) == false) {
          arr.push(formatDate(newDate))
      }
    // Use UTC date to prevent problems with time zones and DST
      currentDate.setUTCDate(currentDate.getUTCDate() + steps)
  }
  for(let i = 1; i<arr.length; i++){
      if(blist.includes(arr[i]) == false) {
          blist.push(formatDate(arr[i]))
      }
      if(list2.includes(arr[i]) == false){
              list2.push(formatDate(arr[i]))
          }
      console.log("bwaerme "+i+ " "+ blist)
  }
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}


function StatistikBehGrafik(chart){
    for (let i = 0; i < bdatevons.length; i++) {
        if (labels.includes(bdatevons[i]) == false) {
                labels.push(bdatevons[i]);
                labels.sort();
                var index = labels.indexOf(bdatevons[i]);

                for (let l = labels.length; l >= index; l--) {
                    for (let d = 0; d < chart.data.datasets.length; d++) {
                        chart.data.datasets[d].data[l] = chart.data.datasets[d].data[l - 1]
                        if (l == index) {
                            chart.data.datasets[d].data[index] = null;
                        }
                    }
                }
            }
    }
    DefineDataSet(chart, "Entnahme von Drohnenbrut", b_drohnenbrut, "#a564d3")
    DefineDataSet(chart, "Wärmebehandlung", b_waerme, "#b66ee8")
    DefineDataSet(chart, "Bannwabenverfahren", b_bannwaben, "#c879ff")
    DefineDataSet(chart, "Ablegerbildung", b_ablegerbildung, "#d689ff")
    DefineDataSet(chart, "Entsorgen der Brutwaben", b_entsorgungBrut, "#e498ff")
    DefineDataSet(chart, "organische Phosphorsäureester", b_phosphor, "#f2a8ff")
    DefineDataSet(chart, "Pyrethroide", b_pyrethroide, "#ffb7ff")
    DefineDataSet(chart, "Metarhizium-Pilz", b_metarhizium, "#ffc4ff")
    DefineDataSet(chart, "Bücherskorpion", b_buecher, "#ffc9ff")
    DefineDataSet(chart, "Milchsäure sprühen", b_milchsaeure, "#ffceff")
    DefineDataSet(chart, "ätherische Öle", b_aetherischoele, "#b28dff")
    DefineDataSet(chart, "Ameisensäure Schwammtuch", b_asSchwamm, "#bc98ff")
    DefineDataSet(chart, "Ameisensäure Universal-Verdunster", b_asUniversal, "#c5a3ff")
    DefineDataSet(chart, "Ameisensäure Liebig-Verdunster", b_asLiebigv, "#d1b1ff")
    DefineDataSet(chart, "Ameisensäure Nassenheider-Verdunster", b_asNassenv, "#d7b8ff")
    DefineDataSet(chart, "Oxalsäure träufeln", b_oTraeufeln, "#dcbfff")
    DefineDataSet(chart, "Oxalsäure sprühen", b_oSpruehen, "#e3c7ff")
    DefineDataSet(chart, "Oxalsäure E-Verdampfer mit Thermostat", b_oEVerdMitT, "#e9cfff")
    DefineDataSet(chart, "Oxalsäure E-Verdampfer ohne Thermostat", b_oEVerdOhneT, "#f2daff")
    DefineDataSet(chart, "Oxalsäure Gas-Verdampfer", b_oGasverdampfer, "#fbe4ff")
    DefineDataSet(chart, "Oxalsäure Vernebler", b_oVernebler, "#b9b5ff")
    DefineDataSet(chart, "natürliche Evolution", b_natuerlicheE, "#afb1ff")
}
function DefineDataSet(chart, behandlung, liste, farbe){
    bdata = []
        if (liste.length > 0) {
            for(let a = 0; a<liste.length; a++){
                for (let l = 0; l < labels.length; l++) {
                    if (liste[a] == labels[l]) {
                        console.log(l)
                        console.log(liste[a])
                        console.log(labels[l])
                        bdata[l] = 10
                    }
                }
            for (let l = 0; l < labels.length; l++) {
                if (bdata[l] == null) {
                    bdata[l] = 0;
                }
            }

        }
            console.log(bdata)
            newDataSet = {
                label: behandlung,
                backgroundColor: farbe,
                borderColor: farbe,
                type: "bar",
                data: bdata
            };
            chart.data.datasets.push({
                borderColor: newDataSet.borderColor,
                backgroundColor: newDataSet.backgroundColor,
                data: newDataSet.data,
                label: newDataSet.label,
                fill: true,
            });
    }
}
function HideData(){
    let chart = document.getElementById("myChart");
    for (let l = 0; l < labels.length; l++) {
        for (let d = 0; d < chart.data.datasets.length; d++) {
            let type = chart.data.datasets[d].data[l].type;
            if(type == "bar"){
                chart.options = {
                    events: []
                }
                return false
            }
            else {
                chart.options = {
                    animation: {
                duration: 0 // general animation time
            },
            hover: {
                animationDuration: 0 // duration of animations when hovering an item
            },
            responsiveAnimationDuration: 0, // animation duration after a resize
                    tooltips: {
                        enabled: true
                    }
                }
                return true
            }
        }
    }
    // events: (HideData == false) ? []:["mousemove", "mouseout", "click", "touchstart", "touchmove", "touchend"]
}


function menuezeile(){
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let bild = urlParams.get('bild');
    let token = urlParams.get('token');
     check_token(token,name);
    document.getElementById('profilimg').src = "../statics/img/" + bild + ".png";
    document.getElementById('einst').href="einstellungen.html?name="+name+"&bild="+bild+"&token="+token;
    document.getElementById('stat').href="statistik.html?name="+name+"&bild="+bild+"&token="+token;
    document.getElementById('home').href="home.html?name="+name+"&bild="+bild+"&token="+token;
    document.getElementById('profil').href="profil.html?name="+name+"&bild="+bild+"&token="+token;
}
function check_token(token,name){
    var xhttp = new XMLHttpRequest();
    var check = false;
    var weiter = true;
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var tokens = JSON.parse(this.responseText);
            console.log(tokens)
            for (var i = 0; i < tokens.length; i++) {
                     if (tokens[i].t_id == token && tokens[i].t_email == name) {
                         console.log(tokens[i].t_datetime);
                         if(check_datetime(tokens[i].t_datetime)==true){
                             check = true;
                         }
                     }
                 }
            if(check == false){
                 window.location.href = "index.html";
                window.alert("Dieser Link ist leider ungültig. Das könnte daran liegen, dass deine Sitzung abgelaufen ist! Bitte melde dich nochmal an.");


            }
        }
    };
    xhttp.open("GET", linkurl+"/tokens", true);
    xhttp.send();
}
function check_datetime(date){
    var jetzt = new Date();
    var d = new Date(date);
    var time = d.getTime();
    if(jetzt.getTime() <= 10800000) time = time + 86400000;
    var dif = jetzt.getTime() - time;
    var check = false;
    if(d.getDate() == jetzt.getDate() || d.getDate() +1 == jetzt.getDate())
        if(dif < 10800000) check = true;
    return check;
}