const linkurl = "http://37.221.195.234:5050";
const linkurle = "http://37.221.195.234";
class User {
  constructor(u_name, u_hash, u_salt, u_email, u_telefon, u_bild) {
    this.u_name = u_name;
    this.u_hash = u_hash;
    this.u_salt = u_salt;
    this.u_email = u_email;
    this.u_telefon = u_telefon;
    this.u_bild = u_bild;
  }
};

function login(){
    var bn = document.getElementById("login_bn").value;
    var pw = document.getElementById("login_pw").value;
    var xhttp = new XMLHttpRequest();
    var user = undefined;
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            users = JSON.parse(this.responseText);
            for (var i = 0; i < users.length; i++) {
                var hash = crypt_password(pw + "" + users[i].u_salt);
                if (users[i].u_name == bn && hash == users[i].u_hash) {
                    user = users[i];
                }
            }
            console.log();
            if (user != undefined) {
                token_erstellen(bn, user.u_bild);
            } else window.alert("Ungültige Daten! Versuche es nochmal!")
        }
    };
    xhttp.open("GET", linkurl + "/users", true);
    xhttp.send();
};


function show_pw(spw,pw){
    var checkbox =  document.getElementById(spw);
    if(checkbox.checked == true) {
        document.getElementById(pw).type = "text";
    }
    else {
        document.getElementById(pw).type = "password";
    }
}

function post_token(token,email){
    var t = new Token(token,email, new Date());
    var xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
        console.log(this.responseText);

    };
    xhttp.open("POST", linkurl+"/token", true);
    xhttp.send(JSON.stringify(t));
}

function token_erstellen(email, bild){
    var t ="";
    var xhttp = new XMLHttpRequest();
    var check = false;
    var weiter = true;
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var tokens = JSON.parse(this.responseText);
            console.log(tokens)
             while(weiter ==true) {
                 var r = Math.random();
                 t = crypt_password(r + "" + email);
                 for (var i = 0; i < tokens.length; i++) {
                     console.log(tokens[i].t_id + " - " + t);
                     if (tokens[i].t_id == t) {
                         check = true;
                     }
                 }
                 weiter = check;
             }
             post_token(t,email);
            window.location.href = "home.html?name="+email+"&bild="+bild+"&token="+t+"";
        }
    };
    xhttp.open("GET", linkurl +"/tokens", true);
    xhttp.send();
}


function register() {
    var bn = document.getElementById("r_bn").value;
    var pw = document.getElementById("r_pw").value;
    var wpw = document.getElementById("r_wpw").value;
    var email = document.getElementById("r_em").value;
    var tel = document.getElementById("r_tel").value;
    var img = document.getElementById("akt_img").innerHTML;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            users = JSON.parse(this.responseText);
            if (check_bn(bn, users) == true) {
                if (check_pw(pw, wpw) == true) {
                    if (check_email(email) == true) {
                        if (check_em(email, users) == true) {
                            if (check_tel(tel) == true) {
                                var salt = Math.random();
                                var u = new User(bn, crypt_password(pw + "" + salt), salt, email, tel, img);
                                post_user(u);

                                registrierung_email();
                            } else window.alert("Bitte gebe eine gültige Telefonnummer ein!")
                        } else window.alert("Diese Email-Adresse ist leider schon vergeben. Bitte wähle eine Email-Adresse oder melde dich mit deinem Accout an!");
                    } else window.alert("Bitte gebe eine gültige Email-Adresse ein!");
                }
            } else window.alert("Den Benutzernamen gibt es schon. Bitte wähle einen anderen Usernamen!")
        }
    };
    xhttp.open("GET", linkurl +"/users", true);
    xhttp.send();
};

function post_user(u) {
    var xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
        console.log(this.responseText);
        post_settings(u.u_name);

    };
    xhttp.open("POST", linkurl +"/user", true);
    xhttp.send(JSON.stringify(u));

};

function post_settings(u_name){
    var e = new Einstellung(u_name,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1);
    var xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
        console.log(this.responseText);
    };
    xhttp.open("POST", linkurl+"/setting/"+u_name, true);
    xhttp.send(JSON.stringify(e));

}

function check_bn(bn, users) {
    var check = true;
    for (var i = 0; i < users.length; i++) {
        //Maria DB ist nicht case Sensitiv, Javascript schon
        if (users[i].u_name.toUpperCase() == bn.toUpperCase()) {
            check = false;
        }
    }
    return check;

};

function check_em(em, users) {
    var check = true;
    for (var i = 0; i < users.length; i++) {
        if (users[i].u_email == em) {
            check = false;
        }
    }
    return check;
};

function check_pw(pw,wpw){
    var fehlermeldung = "";
    var check = true;
    if(pw != wpw) fehlermeldung = (fehlermeldung + "Die beiden Paswörter stimmen nicht überein! \n");
    if( pw.length < 8) fehlermeldung = (fehlermeldung +"Das Passwort ist zu kurz! Es muss mindestens 8 Zeichen haben! \n");
    var sz = false;
    var sonderzeichen = '!"§$%&/()=?{[]}+*#-.,_:;><|@°€';
    var gb = false;
    var gbuchstaben = 'ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ';
    var z = false;
    var zahlen = '1234567890';
    for (var i = 0; i<pw.length; i++){
        for(var x= 0; x <gbuchstaben.length; x++){
             if(pw[i] == gbuchstaben[x]) gb = true;
        }
        for(var x = 0; x <sonderzeichen.length; x++){
            if (pw[i] == sonderzeichen[x]) sz = true;
        }
        for(var x = 0; x <zahlen.length; x++){
            if (pw[i] == zahlen[x]) z = true;
        }
    }
    if( sz == false) fehlermeldung = (fehlermeldung +"Das Passwort muss mindestens ein Sonderzeichen enthalten!\n");
    if( gb == false) fehlermeldung = (fehlermeldung +"Das Passwort muss mindestens einen Großbuchstaben enthalten!\n");
    if( z == false) fehlermeldung = (fehlermeldung +"Das Passwort muss mindestens eine Zahl enthalten!\n");
    if (fehlermeldung != ""){
        check = false;
        window.alert(fehlermeldung);
    }
    return check;
}

function check_email(em){
    var check = false;
    var at = false;
    var atindex = null;
    var dotindex = null;
    var dot = false;
    for (var i = 0; i<em.length; i++){
        if(at ==false) {
            if(em[i] == "@") {
                at = true;
                atindex = i;
            }
        }
        else{
            if(em[i] == ".") {
                dot = true;
                dotindex = i;
            }
        }
    }
    if (dot == true){
        var x = dotindex - atindex;
        var y = em.length - dotindex;
        if(x > 1 && y >1 && atindex > 0) check = true;
    }
    return check;
}

function check_tel(t){
    var check = true;
    var zahlen = '1234567890 ';
    var i =0;
    if(t[0]== "+") i =1;
    for (i; i<t.length; i++){
        var c = false;
        for(var x = 0; x <zahlen.length; x++){
            if (t[i] == zahlen[x])  c = true;
        }
        if(c == false) check = false;
    }
    return check;
}




class Einstellung{
    constructor(u, prop, wirr, stetig, sanft, hw, bw, pw, dw, lw, mw,ass,
                asu, asl, asn, ost,oss,oset,oso,osg,osn, drohnenent,bann,milch,
                waerme, ableger, brutent, op, py, mp,bs, ao ) {
        this.u = u;
        this.prop = prop;
        this.wirr = wirr;
        this.stetig = stetig;
        this.sanft = sanft;
        this.hw = hw;
        this.bw = bw;
        this.pw = pw;
        this.dw = dw;
        this.lw = lw;
        this.mw = mw;
        this.ass = ass;
        this.asu = asu;
        this.asl = asl;
        this.asn = asn;
        this.ost = ost;
        this.oss = oss;
        this.oset = oset;
        this.oso = oso;
        this.osg = osg;
        this.osn = osn;
        this.drohnenent = drohnenent;
        this.bann = bann;
        this.milch = milch;
        this.waerme = waerme;
        this.ableger = ableger;
        this.brutent = brutent;
        this.op = op;
        this.py = py;
        this.mp = mp;
        this.bs = bs;
        this.ao = ao;
    }
}

function einstellungen() {
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let bild = urlParams.get('bild');
    menuezeile();
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var einstellung = JSON.parse(this.responseText);
            console.log(einstellung);
            if(einstellung.es_honigw == "1") document.getElementById('einst_honig').checked = true;
            else document.getElementById('einst_honig').checked = false;
            if(einstellung.es_pollenw == "1") document.getElementById('einst_pollen').checked = true;
            else document.getElementById('einst_pollen').checked = false;
            if(einstellung.es_brutw == "1") document.getElementById('einst_brut').checked = true;
            else document.getElementById('einst_brut').checked = false;
            if(einstellung.es_leerw == "1") document.getElementById('einst_leer').checked = true;
            else document.getElementById('einst_leer').checked = false;
            if(einstellung.es_drohnenw == "1") document.getElementById('einst_drohnen').checked = true;
            else document.getElementById('einst_drohnen').checked = false;
            if(einstellung.es_mittelw == "1") document.getElementById('einst_mittel').checked = true;
            else document.getElementById('einst_mittel').checked = false;
            if(einstellung.es_propolis == "1") document.getElementById('einst_propolis').checked = true;
            else document.getElementById('einst_propolis').checked = false;
            if(einstellung.es_wirrbau == "1") document.getElementById('einst_wirr').checked = true;
            else document.getElementById('einst_wirr').checked = false;
            if(einstellung.es_sanftmut == "1") document.getElementById('einst_sanft').checked = true;
            else document.getElementById('einst_sanft').checked = false;
            if(einstellung.es_stetigkeit == "1") document.getElementById('einst_stetig').checked = true;
            else document.getElementById('einst_stetig').checked = false;

            document.getElementById('einst_asl').checked = get_bool_wert(einstellung.es_ameisensaeure_liebig_verdunster) ;
            document.getElementById('einst_asn').checked = get_bool_wert(einstellung.es_ameisensaeure_nassenheider_verdunster );
            document.getElementById('einst_ass').checked = get_bool_wert(einstellung.es_ameisensaeure_schwammtuch);
            document.getElementById('einst_asu').checked = get_bool_wert(einstellung.es_ameisensaeure_universal_verdunster);

            document.getElementById('einst_oset').checked = get_bool_wert(einstellung['es_oxalsaeure_e-verdampfer_mit_thermostat']);
            if(einstellung.es_oxalsaeure_gas_verdampfer == "1") document.getElementById('einst_osg').checked = true;
            else document.getElementById('einst_osg').checked = false;
            if(einstellung.es_oxalsaeure_vernebler == "1") document.getElementById('einst_osn').checked = true;
            else document.getElementById('einst_osn').checked = false;
            if(einstellung['es_oxalsaeure_e-verdampfer_ohne_thermostat'] == "1") document.getElementById('einst_oso').checked = true;
            else document.getElementById('einst_oso').checked = false;
            if(einstellung.es_oxalsaeure_spruehen == "1") document.getElementById('einst_oss').checked = true;
            else document.getElementById('einst_oss').checked = false;
            if(einstellung.es_oxalsaeure_traeufeln == "1") document.getElementById('einst_ost').checked = true;
            else document.getElementById('einst_ost').checked = false;

            if(einstellung.es_milchsaeure == "1") document.getElementById('einst_milch').checked = true;
            else document.getElementById('einst_milch').checked = false;
            if(einstellung.es_drohnenbn == "1") document.getElementById('einst_drohenent').checked = true;
            else document.getElementById('einst_drohenent').checked = false;
            if(einstellung.es_waermebehandlung == "1") document.getElementById('einst_waerme').checked = true;
            else document.getElementById('einst_waerme').checked = false;
            if(einstellung.es_ablegerbildung == "1") document.getElementById('einst_ableger').checked = true;
            else document.getElementById('einst_ableger').checked = false;
            if(einstellung.es_brutwabenentsorgung == "1") document.getElementById('einst_brutent').checked = true;
            else document.getElementById('einst_brutent').checked = false;
            if(einstellung.es_phosphorsaeureester == "1") document.getElementById('einst_op').checked = true;
            else document.getElementById('einst_op').checked = false;
            if(einstellung.es_pyrethroide == "1") document.getElementById('einst_py').checked = true;
            else document.getElementById('einst_py').checked = false;
            if(einstellung['es_metarhizium-pilz'] == "1") document.getElementById('einst_mp').checked = true;
            else document.getElementById('einst_mp').checked = false;
            if(einstellung.es_buecherskorpion == "1") document.getElementById('einst_bs').checked = true;
            else document.getElementById('einst_bs').checked = false;
            if(einstellung['es_aetherische-oele'] == "1") document.getElementById('einst_ao').checked = true;
            else document.getElementById('einst_ao').checked = false;
            if(einstellung.es_bannwaben == "1") document.getElementById('einst_bann').checked = true;
            else document.getElementById('einst_bann').checked = false;
        }
    };
    xhttp.open("GET", linkurl+"/setting/" + name, true);
    xhttp.send();
};



function save_ceckbox(){
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    var xhttp = new XMLHttpRequest();
    var hw = get_binaer_wert('einst_honig');
    var pw = get_binaer_wert('einst_pollen');
    var bw = get_binaer_wert('einst_brut');
    var lw = get_binaer_wert('einst_leer');
    var dw = get_binaer_wert('einst_drohnen');
    var mw = get_binaer_wert('einst_mittel');
    var prop = get_binaer_wert('einst_propolis');
    var wirr = get_binaer_wert('einst_wirr');
    var sanft = get_binaer_wert('einst_sanft');
    var stetig= get_binaer_wert('einst_stetig');

    var ass =get_binaer_wert('einst_ass');
    var asu =get_binaer_wert('einst_asu');
    var asl =get_binaer_wert('einst_asl');
    var asn =get_binaer_wert('einst_asn');

    var ost =get_binaer_wert('einst_ost');
    var oss =get_binaer_wert('einst_oss');
    var oset =get_binaer_wert('einst_oset');
    var oso =get_binaer_wert('einst_oso');
    var osg =get_binaer_wert('einst_osg');
    var osn =get_binaer_wert('einst_osn');

    var milch= get_binaer_wert('einst_milch');
    var drohnent= get_binaer_wert('einst_drohenent');
    var bann=get_binaer_wert('einst_bann');
    var waerme=get_binaer_wert('einst_waerme');
    var ableger =get_binaer_wert('einst_ableger');
    var brutent =get_binaer_wert('einst_brutent');
    var op =get_binaer_wert('einst_op');
    var py =get_binaer_wert('einst_py');
    var mp =get_binaer_wert('einst_mp');
    var bs =get_binaer_wert('einst_bs');
    var ao =get_binaer_wert('einst_ao');
    var e = new Einstellung(name,prop,wirr,stetig,sanft,hw,bw,pw,dw,lw,mw,ass,asu,asl,asn,ost,oss,oset,oso,osg,osn,drohnent,bann,milch,waerme,ableger,brutent,op,py,mp,bs,ao);
    var xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
        console.log(this.responseText);
    };
    xhttp.open("PUT", linkurl+"/setting/" + name, true);
    xhttp.send(JSON.stringify(e));
}

function get_binaer_wert(feld){
    if(document.getElementById(feld).checked == true) return 1;
    else return 0;
}
function get_bool_wert(feld){
    if(feld == '1') return true;
    else return false;
}


function check_all(check, array){
    var c = document.getElementById(check).checked;
    for(var i =0; i<array.length; i++){
        document.getElementById(array[i]).checked = c;
    }
}

function profil(){
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let bild = urlParams.get('bild');
    let token = urlParams.get('token');
    document.getElementById('pr_img').src="../statics/img/"+bild+".png";
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        var user = JSON.parse(this.responseText);
        console.log(user);
        document.getElementById('pr_bn').value = name;
        document.getElementById('pr_em').value = user.u_email;
        document.getElementById('pr_oldbn').value = name;
        document.getElementById('pr_oldem').value = user.u_email;
        document.getElementById('pr_tel').value = user.u_telefon;
        }
    };
    xhttp.open("GET", linkurl+"/user/" + name, true);
    xhttp.send();
    menuezeile();
    document.getElementById('pr_pb').href = "profilbild_aendern.html?name="+name+"&bild="+bild+"&token="+token;
    document.getElementById('pr_pw').href = "passwort_aendern.html?name="+name+"&bild="+bild+"&token="+token;
}

function profil_speichern(){
    var bn = document.getElementById('pr_bn').value;
    var em =  document.getElementById('pr_em').value;
    var old_bn = document.getElementById('pr_oldbn').value;
    var old_em =  document.getElementById('pr_oldem').value;
    var tel = document.getElementById('pr_tel').value;
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let bild = urlParams.get('bild');
    let token = urlParams.get('token');
    var xhttp2 = new XMLHttpRequest();
    xhttp2.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var users = JSON.parse(this.responseText);
            if( check_bn(bn,users)==true || bn == old_bn) {
            if(check_email(em)==true){
                if(check_em(em,users) == true  || em == old_em) {
                    if(check_tel(tel)==true) {
                    window.alert("Änderung erfolgreich!");
                    var xhttp = new XMLHttpRequest();
                    xhttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        var user = JSON.parse(this.responseText);
                        console.log(user);
                         var u = new User(bn, user.u_hash, user.u_salt, em, tel, user.u_bild);
                         console.log(u);
                         put_user(u, old_bn);
                         window.location.href = "profil.html?name="+u.u_name+"&bild="+u.u_bild+"&token="+token;
                        }
                    };
                    xhttp.open("GET", linkurl+"/user/" + name, true);
                    xhttp.send();
                    } else window.alert("Bitte gebe eine gültige Telefonnummer ein!")
                }  else window.alert("Diese Email-Adresse ist leider schon vergeben. Bitte wähle eine Email-Adresse oder melde dich mit deinem Accout an!");
            }else window.alert("Bitte gebe eine gültige Email-Adresse ein!");
        } else window.alert("Den Benutzernamen gibt es schon. Bitte wähle einen anderen Usernamen!")
        }
    };
    xhttp2.open("GET", linkurl+"/users", true);
    xhttp2.send();

}

function put_user(u, bn){
    var xhttp = new XMLHttpRequest();

    console.log("!!!!")
    xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        console.log(this.responseText);
        }
    };
    xhttp.open("PUT", linkurl+"/user/" + bn, true);
    xhttp.send(JSON.stringify(u));

};


function open_passwort_aendern(){
     let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let bild = urlParams.get('bild');
    let token = urlParams.get('token');
    menuezeile()
    document.getElementById('abbrechen').href = "profil.html?name="+name+"&bild="+bild+"&token="+token;

}

function passwort_aendern() {
    var apw = document.getElementById('pae_apw').value;
    var pw = document.getElementById('pae_pw').value;
    var wpw = document.getElementById('pae_wpw').value;
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let bild = urlParams.get('bild');
    let token = urlParams.get('token');
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        var user = JSON.parse(this.responseText);
        if (crypt_password(apw+""+user.u_salt) == user.u_hash) {
            if (check_pw(pw, wpw) == true) {
                var salt = Math.random();
                var u = new User(name, crypt_password(pw + "" + salt),salt, user.u_email, user.u_telefon, user.u_bild);
                put_user(u, name,);
                window.alert("Passwort geändet auf: " + pw);
                window.location.href = "profil.html?name="+name+"&bild="+bild_neu+"&token="+token;

            }

        }else window.alert("Dein altes Passwort ist leider falsch!")
        }
    };
    xhttp.open("GET", linkurl+"/user/" + name, true);
    xhttp.send();
}


function crypt_password(s){
    return s.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0);
}


function profilbild_kennzeichnen(img){
    let urlParams = new URLSearchParams(window.location.search);
    let akt_bild = document.getElementById("akt_img").innerHTML;
    document.getElementById(akt_bild).height = 100;
    document.getElementById(img).height = 110;
    document.getElementById("akt_img").innerHTML = img;
    console.log(img);
}

function open_profilbild_aendern(){
    menuezeile();
    let urlParams = new URLSearchParams(window.location.search);
    let bild = urlParams.get('bild');
    let name = urlParams.get('name');
    let token = urlParams.get('token');
    document.getElementById(bild).height = 140;
    document.getElementById("akt_img").innerHTML = bild;
    document.getElementById("abbrechen").href= "profil.html?name="+name+"&bild="+bild+"&token="+token;
}
function profilbild_aendern(){
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let bild_neu = document.getElementById("akt_img").innerHTML;
    let token = urlParams.get('token');
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        var user = JSON.parse(this.responseText);
        console.log(user);
        var u = new User(name, user.u_hash, user.u_salt, user.u_email, user.u_telefon, bild_neu);
        console.log(u);
        put_user(u, name);
        window.location.href = "profil.html?name="+name+"&bild="+bild_neu+"&token="+token;
        }
    };
    xhttp.open("GET", linkurl+"/user/" + name, true);
    xhttp.send();
}



class Token {
  constructor(t_id, t_email, t_datetime) {
    this.t_id = t_id;
    this.t_email = t_email;
    this.t_datetime = t_datetime;
  }
};

//var pw_noreply = "scyysbimtoqfljtd";
//var pw_office = "ctqvfzlrrldkkdwj";
//Funktioniert nur teilweise
function passwort_vergessen() {
    var em = document.getElementById('pw_email').value;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var users = JSON.parse(this.responseText);
            if (check_email(em) == true) {
                if (check_em(em, users) == false) {
                    var bn = "";
                    for (var i = 0; i < users.length; i++) {
                        if (users[i].u_email == em) {
                            bn = users[i].u_name;

                        }
                    }
                   pv_email(em,bn);
                } else window.alert("Diese Email-Adresse ist noch nicht vergeben. Du kannst dir einen neuen Account unter Registrieren erstellen!");
            } else window.alert("Bitte gebe eine gültige Email-Adresse ein!");
        }
    };
    xhttp.open("GET", linkurl+"/users", true);
    xhttp.send();
};
function pv_email(em,bn){
    var url = new URL(linkurle+'/passwort_wiedererstellen.html');
    var t ="";
    var xhttp = new XMLHttpRequest();
    var check = false;
    var weiter = true;
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var tokens = JSON.parse(this.responseText);
            console.log(tokens)

             while(weiter ==true) {
                 var r = Math.random();
                 t = crypt_password(r + "" + em);
                 for (var i = 0; i < tokens.length; i++) {
                     console.log(tokens[i].t_id + " - " + t);
                     if (tokens[i].t_id == t) {
                         check = true;
                     }
                 }
                 weiter = check;
             }
             url.searchParams.set("token", t);
            url.searchParams.set("name", em);
              Email.send({
                        Host: "Smtp.gmail.com",
                        Username: "noreply.seethebee@gmail.com",
                        Password: "scyysbimtoqfljtd",
                        To: ""+em+"",
                        From: "noreply.seethebee@gmail.com",
                        Subject: "See The Bee - Passwort Wiederherstellung",
                        Body: "<h1>Liebe/r, " + bn + " ("+em+")! </h1> <h3>Du hast dein Passwort für See The Bee vergessen? <br> Klicke auf folgenden Link um ein neues zu erstellen:</h3> <h2><a href="+url+">Passwort ändern</a></h2>",
                    }).then(window.alert("Mail wurde erfolgreich versendet"));
            post_token(t,em);
        }
    };
    xhttp.open("GET", linkurl+"/tokens", true);
    xhttp.send();
}

function registrierung_email() {
    var em = document.getElementById('r_em').value;
    var bn = document.getElementById('r_bn').value;
    var url = new URL(linkurle+'/konto_bestaetigen.html');
    var b = "<h1>Liebe/r, " + bn + " ("+em+")!</h1> <h3>Du hast ein neues Konto auf unserer Imkereiwebsite See The Bee erstellt? <br> Klicke auf folgenden Link um deine Email Adresse zu bestätigen:</h3>";
    token_erstellen_r_email(em,"scyysbimtoqfljtd", "noreply.seethebee@gmail.com", "Willkommen bei 'See The Bee'!", b, url, "Konto bestätigen");

};

function token_erstellen_r_email(email, pw,from,sub,b, url, link){
    var t ="";
    var xhttp = new XMLHttpRequest();
    var check = false;
    var weiter = true;
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var tokens = JSON.parse(this.responseText);
            console.log(tokens)

             while(weiter ==true) {
                 var r = Math.random();
                 t = crypt_password(r + "" + email);
                 for (var i = 0; i < tokens.length; i++) {
                     console.log(tokens[i].t_id + " - " + t);
                     if (tokens[i].t_id == t) {
                         check = true;
                     }
                 }
                 weiter = check;
             }
             url.searchParams.set("token", t);
            url.searchParams.set("name", email);
             Email.send({
                Host: "Smtp.gmail.com",
                Username: from,
                Password: pw,
                To: ""+email+"",
                From: from,
                Subject: sub,
                Body: b + "<h2><a href="+url+">"+link+"</a></h2>",
            }).then (
                        r_alert()
                     );
            post_token(t,email);
        }
    };
    xhttp.open("GET", linkurl+"/tokens", true);
    xhttp.send();
}
function r_alert(){
     window.alert("Deine Registrierung war erfolgreich! \n Wir haben dir eine Email geschickt. Bitte bestätige deine Email-Adresse.")
    window.location.href = "index.html"
}

function load_stf() {
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let bild = urlParams.get('bild');
    let e = urlParams.get('id');
    stf_einstellungen(name);
    if (e != 'null') {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var eintrag = JSON.parse(this.responseText);
                console.log(eintrag);
                document.getElementById('stf_datum').value = eintrag.e_date;
                document.getElementById('stf_von').value = eintrag.e_von;
                document.getElementById('stf_bis').value = eintrag.e_bis;
                document.getElementById('stf_von').value = eintrag.e_von;
                var k = parseInt(eintrag.e_koenigin);
                if(k==1) k=true;
                else k=false;
                document.getElementById('stf_koenigingefunden').checked = k;
                document.getElementById('stf_bemerkung').value = eintrag.e_infos;
                document.getElementById('stf_varroa').value = eintrag.e_varroa;
                document.getElementById('stf_weisel').innerHTML = eintrag.e_weiselz;
                document.getElementById('stf_honigw').innerHTML = eintrag.e_honigw;
                document.getElementById('stf_pollenw').innerHTML = eintrag.e_pollenw;
                document.getElementById('stf_mittelw').innerHTML = eintrag.e_mittelw;
                document.getElementById('stf_brutw').innerHTML = eintrag.e_brutw;
                document.getElementById('stf_drohnenw').innerHTML = eintrag.e_drohnenw;
                document.getElementById('stf_leerw').innerHTML = eintrag.e_leerw;
                select_standorte(name, 'stf_standort', 'stf_stock', 'entry');
                var p = document.getElementsByName('stf_prop');
                p[parseInt(eintrag.e_propolis)-1].checked = true;
                var sanft = document.getElementsByName('stf_sanft');
                sanft[parseInt(eintrag.e_sanftmut)-1].checked = true;
                var stetig = document.getElementsByName('stf_stetig');
                stetig[parseInt(eintrag.e_stetigkeit)-1].checked = true;
                var wirr = document.getElementsByName('stf_wirr');
                wirr[parseInt(eintrag.e_wirrbau)-1].checked = true;
            }
        };
        xhttp.open("GET", linkurl+"/entry/" + e, true);
        xhttp.send();
    } else {
        load_datetime('stf_datum', 'stf_von');
        select_standorte(name, `stf_standort`, 'stf_stock', 'entry');
    }
    menuezeile();
}
function stf_einstellungen(name) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var einstellung = JSON.parse(this.responseText);
            if (einstellung.es_honigw != 1) document.getElementById("stf_honigdiv").style.display = "none";
            else document.getElementById("stf_honigdiv").style.display = "block";
            if (einstellung.es_pollenw != 1) document.getElementById("stf_pollenwdiv").style.display = "none";
            else document.getElementById("stf_pollenwdiv").style.display = "block";
            if (einstellung.es_brutw != 1) document.getElementById("stf_brutwdiv").style.display = "none";
            else document.getElementById("stf_brutwdiv").style.display = "block";
            if (einstellung.es_drohnenw != 1) document.getElementById("stf_drohnenwdiv").style.display = "none";
            else document.getElementById("stf_drohnenwdiv").style.display = "block";
            if (einstellung.es_leerw != 1) document.getElementById("stf_leerwdiv").style.display = "none";
            else document.getElementById("stf_leerwdiv").style.display = "block";
            if (einstellung.es_mittelw != 1) document.getElementById("stf_mittelwdiv").style.display = "none";
            else document.getElementById("stf_mittelwdiv").style.display = "block";


            if (einstellung.es_propolis != 1) document.getElementById("stf_propdiv").style.display = "none";
            else document.getElementById("stf_propdiv").style.display = "block";
            if (einstellung.es_wirrbau != 1) document.getElementById("stf_wirrdiv").style.display = "none";
            else document.getElementById("stf_wirrdiv").style.display = "block";
            if (einstellung.es_stetigkeit != 1) document.getElementById("stf_stetigdiv").style.display = "none";
            else document.getElementById("stf_stetigdiv").style.display = "block";
            if (einstellung.es_sanftmut != 1) document.getElementById("stf_sanftdiv").style.display = "none";
            else document.getElementById("stf_sanftdiv").style.display = "block";
        }
        ;
    }
    xhttp.open("GET", linkurl+"/setting/" + name, true);
    xhttp.send();

}


function load_bf() {
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let bild = urlParams.get('bild');
    let b = urlParams.get('id');
    bf_einstellungen(name);
    if (b == 'null') {
        load_datetime('bf_datumvon', 'bf_uhrzeitvon');
        select_standorte(name, `bf_standort`, 'bf_stock', 'treatment');
    } else {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange =function () {
            if (this.readyState == 4 && this.status == 200) {
                var behandlung = JSON.parse(this.responseText);
                console.log(behandlung);
                document.getElementById('bf_datumvon').value = behandlung.b_datevon;
                document.getElementById('bf_uhrzeitvon').value = behandlung.b_von;
                document.getElementById('bf_datumbis').value = behandlung.b_datebis;
                document.getElementById('bf_uhrzeitbis').value = behandlung.b_bis;
                document.getElementById('bf_behanldung').value = behandlung.b_behandlung;
                select_standorte(name, 'bf_standort', 'bf_stock', 'treatment');
                document.getElementById('bf_bemerkung').value = behandlung.b_infos;
            }
        };
        xhttp.open("GET", linkurl+"/treatment/" + b, true);
        xhttp.send();
    }
    menuezeile();
};

function bf_einstellungen(name){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var einstellung = JSON.parse(this.responseText);
            if (einstellung.es_drohnenbn != 1) document.getElementById("bf_drohnenent").style.display = "none";
            else document.getElementById("bf_drohnenent").style.display = "block";
            if (einstellung.es_bannwaben != 1) document.getElementById("bf_bann").style.display = "none";
            else document.getElementById("bf_bann").style.display = "block";
            if (einstellung.es_milchsaeure != 1) document.getElementById("bf_ms").style.display = "none";
            else document.getElementById("bf_ms").style.display = "block";
            if (einstellung.es_waermebehandlung != 1) document.getElementById("bf_waerme").style.display = "none";
            else document.getElementById("bf_waerme").style.display = "block";
            if (einstellung.es_ablegerbildung != 1) document.getElementById("bf_ableger").style.display = "none";
            else document.getElementById("bf_ableger").style.display = "block";
            if (einstellung.es_brutwabenentsorgung != 1) document.getElementById("bf_brutent").style.display = "none";
            else document.getElementById("bf_brutent").style.display = "block";
            if (einstellung.es_phosphorsaeureester != 1) document.getElementById("bf_ph").style.display = "none";
            else document.getElementById("bf_ph").style.display = "block";
            if (einstellung.es_pyrethroide != 1) document.getElementById("bf_py").style.display = "none";
            else document.getElementById("bf_py").style.display = "block";
            if (einstellung['es_metarhizium-pilz'] != 1) document.getElementById("bf_mp").style.display = "none";
            else document.getElementById("bf_mp").style.display = "block";
            if (einstellung.es_buecherskorpion != 1) document.getElementById("bf_bs").style.display = "none";
            else document.getElementById("bf_bs").style.display = "block";
            if (einstellung['es_aetherische-oele'] != 1) document.getElementById("bf_ao").style.display = "none";
            else document.getElementById("bf_ao").style.display = "block";
            if (einstellung.es_ameisensaeure_schwammtuch != 1) document.getElementById("bf_ass").style.display = "none";
            else document.getElementById("bf_ass").style.display = "block";
            if (einstellung.es_ameisensaeure_universal_verdunster != 1) document.getElementById("bf_asu").style.display = "none";
            else document.getElementById("bf_asu").style.display = "block";
            if (einstellung.es_ameisensaeure_liebig_verdunster != 1) document.getElementById("bf_asl").style.display = "none";
            else document.getElementById("bf_asl").style.display = "block";
            if (einstellung.es_ameisensaeure_nassenheider_verdunster != 1) document.getElementById("bf_asn").style.display = "none";
            else document.getElementById("bf_asn").style.display = "block";
            if (einstellung.es_oxalsaeure_traeufeln != 1) document.getElementById("bf_ost").style.display = "none";
            else document.getElementById("bf_ost").style.display = "block";
            if (einstellung.es_oxalsaeure_spruehen != 1) document.getElementById("bf_oss").style.display = "none";
            else document.getElementById("bf_oss").style.display = "block";
            if (einstellung['es_oxalsaeure_e-verdampfer_mit_thermostat'] != 1) document.getElementById("bf_oset").style.display = "none";
            else document.getElementById("bf_oset").style.display = "block";
            if (einstellung['es_oxalsaeure_e-verdampfer_ohne_thermostat'] != 1) document.getElementById("bf_oseot").style.display = "none";
            else document.getElementById("bf_oseot").style.display = "block";
            if (einstellung.es_oxalsaeure_gas_verdampfer != 1) document.getElementById("bf_osg").style.display = "none";
            else document.getElementById("bf_osg").style.display = "block";
            if (einstellung.es_oxalsaeure_vernebler != 1) document.getElementById("bf_osv").style.display = "none";
            else document.getElementById("bf_osv").style.display = "block";
        }
        ;
    }
    xhttp.open("GET", linkurl+"/setting/" + name, true);
    xhttp.send();

}

function select_standorte(name, feld, feld2, use){
    var xhttp = new XMLHttpRequest();
    var text ="";
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var standorte = JSON.parse(this.responseText);
            console.log(standorte);
            for(var i =0;i<standorte.length;i++){
                if(standorte[i].st_status == 1) text = text + "<option value='"+standorte[i].st_id+"'>"+standorte[i].st_name+"</option>"
            }
            document.getElementById(feld).innerHTML = text;
            if (id != 'null'){
                set_standort(id, feld,feld2, use)
            }else select_stock(feld,feld2, use);
        }
    };
    xhttp.open("GET", linkurl+"/orte/user/"+name, true);
    xhttp.send();
    let urlParams = new URLSearchParams(window.location.search);
    let id = urlParams.get('id');
}

function select_stock(standort, feld, use){
    var xhttp = new XMLHttpRequest();
    var text ="";
    var st = document.getElementById(standort).value;
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var stoecke = JSON.parse(this.responseText);
            console.log(stoecke);
            for(var i =0;i<stoecke.length;i++){
                if(stoecke[i].s_status == 1) text = text + "<option value='"+stoecke[i].s_id+"'>"+stoecke[i].s_name+"</option>"
            }
            document.getElementById(feld).innerHTML = text;
        }
    };
    xhttp.open("GET", linkurl+"/stoecke/ort/"+st, true);
    xhttp.send();
    let urlParams = new URLSearchParams(window.location.search);
    let id = urlParams.get('id');
    if (id != 'null'){
        set_stock(id, standort,feld, use)
    }
}

function set_stock(b,st, s, use) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var formular = JSON.parse(this.responseText);
            console.log(formular);
            if(use == 'entry'){
                var stock = parseInt(formular.e_stoecke);
                document.getElementById(s).value = stock;
                console.log(stock);
            }
            else if(use ='treatment'){
                var stock = parseInt(formular.b_stoecke);
                document.getElementById(s).value = stock;
            }
        }
    };
    xhttp.open("GET", linkurl+"/"+use+"/" + b, true);
    xhttp.send();
}
function set_standort(b,st, s, use) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var formular = JSON.parse(this.responseText);
            console.log(formular);
            if(use == 'entry'){
                var standort = parseInt(formular.e_standorte);
                document.getElementById(st).value = standort;
            }
            else if(use ='treatment'){
                var standort = parseInt(formular.b_standorte);
                document.getElementById(st).value = standort;
            }
            select_stock(st,s, use);
        }
    };
    xhttp.open("GET", linkurl+"/"+use+"/" + b, true);
    xhttp.send();
}

function load_datetime(date, time){
    let heute = new Date();
    let datum = heute.toLocaleDateString('en-CA');
    let uhrzeit = heute.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
    console.log( datum +' - '+uhrzeit)
    document.getElementById(date).value = datum;
    document.getElementById(time).value = uhrzeit;
}

function count_up(feld){
    var current = parseInt(document.getElementById(feld).innerHTML);
    var neu = current + 1;
    document.getElementById(feld).innerHTML=neu;
}

function count_down(feld){
    var current = parseInt(document.getElementById(feld).innerHTML);
    var neu = current;
    if(current >0) neu = current - 1;
    document.getElementById(feld).innerHTML=neu;
}

class Eintrag {
    constructor(e_date, e_honigw, e_brutw,e_pollenw, e_drohnenw, e_leerw,  e_mittelw,e_koenigin,e_propolis,e_wirrbau, e_stetigkeit,
                e_sanftmut, e_weiselz,e_varroa,e_infos,e_stoecke,e_standorte,e_user, e_von, e_bis ) {
        this.e_date = e_date;
        this.e_honigw = e_honigw;
        this.e_brutw = e_brutw;
        this.e_pollenw = e_pollenw;
        this.e_drohnenw = e_drohnenw;
        this.e_leerw =e_leerw;
        this.e_mittelw = e_mittelw;
        this.e_koenigin =e_koenigin;
        this.e_propolis =e_propolis;
        this.e_wirrbau =e_wirrbau;
        this.e_stetigkeit = e_stetigkeit;
        this.e_sanftmut = e_sanftmut;
        this.e_weiselz = e_weiselz;
        this.e_varroa = e_varroa;
        this.e_infos = e_infos;
        this.e_stoecke = e_stoecke;
        this.e_standorte = e_standorte;
        this.e_user = e_user;
        this.e_von = e_von;
        this.e_bis = e_bis;
    }
};

class Behandlung{
    constructor(b_datevon, b_von, b_datebis, b_bis, b_behandlungen, b_user, b_standorte, b_infos, b_stoecke) {
        this.b_datevon = b_datevon;
        this.b_von = b_von;
        this.b_bis = b_bis;
        this.b_datebis = b_datebis;
        this.b_behandlungen = b_behandlungen;
        this.b_infos = b_infos;
        this.b_stoecke = b_stoecke;
        this.b_standorte = b_standorte;
        this.b_user= b_user;

    }
}

function stf_speichern() {
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let bild = urlParams.get('bild');
    let id = urlParams.get('id');
    let token = urlParams.get('token');
    let stock = document.getElementById('stf_stock').value;
    let standort = document.getElementById('stf_standort').value;
    let st_name = get_standortname(standort);
    let s_name = get_stockname(stock);
    let date = document.getElementById('stf_datum').value;
    let von = document.getElementById('stf_von').value;
    let bis = document.getElementById('stf_bis').value;
    let hw = document.getElementById('stf_honigw').innerHTML;
    let pw = document.getElementById('stf_pollenw').innerHTML;
    let bw = document.getElementById('stf_brutw').innerHTML;
    let dw = document.getElementById('stf_drohnenw').innerHTML;
    let lw = document.getElementById('stf_leerw').innerHTML;
    let mw = document.getElementById('stf_mittelw').innerHTML;
    let varroa = document.getElementById('stf_varroa').value;
    let wz = document.getElementById('stf_weisel').innerHTML;
    let k = document.getElementById('stf_koenigingefunden').checked;
    let koenigin;
    if (k == true) koenigin = 1;
    else koenigin = 0;
    var prop = document.getElementsByName('stf_prop');
    var propolis = get_radio_value(prop);
    var sanft = document.getElementsByName('stf_sanft');
    var sanftmut = get_radio_value(sanft);
    var stetig = document.getElementsByName('stf_stetig');
    var stetigkeit = get_radio_value(stetig);
    var wirr = document.getElementsByName('stf_wirr');
    var wirrbau = get_radio_value(wirr);
    var b = document.getElementById('stf_bemerkung').value;
    let e = new Eintrag(date, hw, bw, pw, dw, lw, mw, koenigin,propolis,wirrbau,stetigkeit,sanftmut,wz,varroa,b,stock,standort,name,von,bis);
    if(id == 'null') stf_post(e,stock);
    else stf_put(e,id);
    window.location.href = "stockEintraege.html?name="+name+"&bild="+bild+"&s_id="+stock+"&s_name="+s_name+"&st_id="+standort+"&st_name="+ st_name+"&token="+token;
}

function get_radio_value(radios){
    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            return radios[i].value;
        }
    }
}

function bf_speichern() {
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let bild = urlParams.get('bild');
    let id = urlParams.get('id');
    let token = urlParams.get('token');
    let stock = document.getElementById('bf_stock').value;
    let standort = document.getElementById('bf_standort').value;
    let st_name = get_standortname(standort);
    let s_name = get_stockname(stock);
    let datumvon = document.getElementById('bf_datumvon').value;
    let timevon = document.getElementById('bf_uhrzeitvon').value;
    let datumbis = document.getElementById('bf_datumbis').value;
    let timebis = document.getElementById('bf_uhrzeitbis').value;
    let behandlung = document.getElementById('bf_behanldung').value;
    let bemerkung = document.getElementById('bf_bemerkung').value;
    let b = new Behandlung(datumvon, timevon, datumbis, timebis, behandlung, name, standort, bemerkung, stock);
    if (id == 'null') bf_post(b, stock);
    else bf_put(b,id);
    window.location.href = "stockEintraege.html?name="+name+"&bild="+bild+"&s_id="+stock+"&s_name="+s_name+"&st_id="+standort+"&st_name="+ st_name + "&token="+token;
};
function stf_post(e,stock) {
    var xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
        console.log(this.responseText);
    };
    xhttp.open("POST", linkurl+"/entry/" + stock, true);
    xhttp.send(JSON.stringify(e));
}

function bf_post(b,stock) {
    var xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
        console.log(this.responseText);
    };
    xhttp.open("POST", linkurl+"/treatment/" + stock, true);
    xhttp.send(JSON.stringify(b));
}

function stf_put(e,id) {
    var xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
        console.log(this.responseText);
    };
    xhttp.open("PUT", linkurl+"/entry/" + id, true);
    xhttp.send(JSON.stringify(e));
}

function bf_put(b,id) {
    var xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
        console.log(this.responseText);
    };
    xhttp.open("PUT", linkurl+"/treatment/" + id, true);
    xhttp.send(JSON.stringify(b));
}

function get_stockname(id){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var stock = JSON.parse(this.responseText);
            return stock.s_name;
        }
    };
    xhttp.open("GET", linkurl+"/stock/" + id, true);
    xhttp.send();
};
function get_standortname(id){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var standort = JSON.parse(this.responseText);
            return standort.st_name;
        }
    };
    xhttp.open("GET", linkurl+"/ort/" + id, true);
    xhttp.send();
};

function loadStand(){
    menuezeile();

}
function loadKoe(){
    menuezeile();

}
function menuezeile(){
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let bild = urlParams.get('bild');
    let token = urlParams.get('token');
     check_token(token,name);
    document.getElementById('profilimg').src = "../statics/img/" + bild + ".png";
    document.getElementById('einst').href="einstellungen.html?name="+name+"&bild="+bild+"&token="+token;
    document.getElementById('stat').href="statistik.html?name="+name+"&bild="+bild+"&token="+token;
    document.getElementById('home').href="home.html?name="+name+"&bild="+bild+"&token="+token;
    document.getElementById('profil').href="profil.html?name="+name+"&bild="+bild+"&token="+token;
}

function check_token(token,name){
    var xhttp = new XMLHttpRequest();
    var check = false;
    var weiter = true;
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var tokens = JSON.parse(this.responseText);
            console.log(tokens)
            for (var i = 0; i < tokens.length; i++) {
                     if (tokens[i].t_id == token && tokens[i].t_email == name) {
                         console.log(tokens[i].t_datetime);
                         if(check_datetime(tokens[i].t_datetime)==true){
                             check = true;
                         }
                     }
                 }
            if(check == false){
                 window.location.href = "index.html";
                window.alert("Dieser Link ist leider ungültig. Das könnte daran liegen, dass deine Sitzung abgelaufen ist! Bitte melde dich nochmal an.");


            }
        }
    };
    xhttp.open("GET", linkurl+"/tokens", true);
    xhttp.send();
}

function check_datetime(date){
    var jetzt = new Date();
    var d = new Date(date);
    var time = d.getTime();
    if(jetzt.getTime() <= 10800000) time = time + 86400000;
    var dif = jetzt.getTime() - time;
    var check = false;
    if(d.getDate() == jetzt.getDate() || d.getDate() +1 == jetzt.getDate())
        if(dif < 10800000) check = true;
    return check;
}

function open_check_token(){
    let urlParams = new URLSearchParams(window.location.search);
    let name = urlParams.get('name');
    let token = urlParams.get('token');
    check_token(token,name);
}


function passwort_wiederherstellen(){
    var pw = document.getElementById("pae_pw").value;
    var wpw = document.getElementById("pae_wpw").value;
    let urlParams = new URLSearchParams(window.location.search);
    let email = urlParams.get('name');
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var users = JSON.parse(this.responseText);
            var user;
            for (var i = 0; i < users.length; i++) {
                if (users[i].u_email == email) {
                    user = users[i];
                }
            }
            if (check_pw(pw, wpw) == true) {
                    var salt = Math.random();
                    var u = new User(user.u_name, crypt_password(pw + "" + salt),salt, user.u_email, user.u_telefon, user.u_bild);
                    put_user(u, user.u_name);
                    window.alert("Passwort geändet auf: " + pw);
                    window.location.href = "index.html";
                }
            }
        };
        xhttp.open("GET", linkurl+"/users", true);
        xhttp.send();
}