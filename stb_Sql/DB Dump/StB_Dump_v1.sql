USE `seethebee`;
-- MySQL dump 10.17  Distrib 10.3.15-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: k165810_seethebee
-- ------------------------------------------------------
-- Server version	10.3.15-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `b_behandlungen`
--

DROP TABLE IF EXISTS `b_behandlungen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `b_behandlungen` (
  `b_id` int(11) NOT NULL AUTO_INCREMENT,
  `b_datetime` date NOT NULL,
  `b_behandlung` enum('Wärmebehandlung','Entnahme von Drohnenbrut','Bannwabenverfahren','Ablegerbildung','Entsorgen der Brutwaben','organische Phosphorsäureester','Pyrethroide','Metarhizium-Pilz','Bücherskorpion','Milchsäure sprühen','ätherische Öle','Ameisensäure Schwammtuch','Ameisensäure Universal-Verdunster','Ameisensäure Liebig-Verdunster','Ameisensäure Nassenheider-Verdunster','Oxalsäure träufeln','Oxalsäure sprühen','Oxalsäure E-Verdampfer mit Thermostat','Oxalsäure E-Verdampfer ohne Thermostat','Oxalsäure Gas-Verdampfer','Oxalsäure Vernebler','natürliche Evolution (ohne Fremdeinwirkung)') NOT NULL,
  `b_dauer` int(11) NOT NULL,
  `b_infos` varchar(100) DEFAULT NULL,
  `b_stoecke` int(11) NOT NULL,
  `b_standorte` int(11) NOT NULL,
  `b_user` varchar(50) NOT NULL,
  PRIMARY KEY (`b_id`),
  KEY `fk_b_behandlungen_s_stoecke1_idx` (`b_stoecke`),
  KEY `fk_b_behandlungen_st_standorte1_idx` (`b_standorte`),
  KEY `fk_b_behandlungen_u_user1_idx` (`b_user`),
  CONSTRAINT `fk_b_behandlungen_s_stoecke1` FOREIGN KEY (`b_stoecke`) REFERENCES `s_stoecke` (`s_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_b_behandlungen_st_standorte1` FOREIGN KEY (`b_standorte`) REFERENCES `st_standorte` (`st_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_b_behandlungen_u_user1` FOREIGN KEY (`b_user`) REFERENCES `u_user` (`u_name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `b_behandlungen`
--

LOCK TABLES `b_behandlungen` WRITE;
/*!40000 ALTER TABLE `b_behandlungen` DISABLE KEYS */;
/*!40000 ALTER TABLE `b_behandlungen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `e_eintraege`
--

DROP TABLE IF EXISTS `e_eintraege`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `e_eintraege` (
  `e_id` int(11) NOT NULL AUTO_INCREMENT,
  `e_datetime` date NOT NULL,
  `e_honigw` int(11) DEFAULT NULL,
  `e_brutw` int(11) DEFAULT NULL,
  `e_pollenw` int(11) DEFAULT NULL,
  `e_drohnenw` int(11) DEFAULT NULL,
  `e_leerw` int(11) DEFAULT NULL,
  `e_mittelw` int(11) DEFAULT NULL,
  `e_koenigin` tinyint(1) DEFAULT 1,
  `e_propolis` enum('1','2','3','4') DEFAULT NULL,
  `e_wirrbau` enum('1','2','3','4') DEFAULT NULL,
  `e_stetigkeit` enum('1','2','3','4') DEFAULT NULL,
  `e_sanftmut` enum('1','2','3','4') DEFAULT NULL,
  `e_weiselz` int(11) DEFAULT NULL,
  `e_varroa` int(11) DEFAULT NULL,
  `e_infos` varchar(100) DEFAULT NULL,
  `e_stoecke` int(11) NOT NULL,
  `e_standorte` int(11) NOT NULL,
  `e_user` varchar(50) NOT NULL,
  PRIMARY KEY (`e_id`),
  KEY `fk_e_eintraege_s_stoecke1_idx` (`e_stoecke`),
  KEY `fk_e_eintraege_st_standorte1_idx` (`e_standorte`),
  KEY `fk_e_eintraege_u_user1_idx` (`e_user`),
  CONSTRAINT `fk_e_eintraege_s_stoecke1` FOREIGN KEY (`e_stoecke`) REFERENCES `s_stoecke` (`s_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_e_eintraege_st_standorte1` FOREIGN KEY (`e_standorte`) REFERENCES `st_standorte` (`st_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_e_eintraege_u_user1` FOREIGN KEY (`e_user`) REFERENCES `u_user` (`u_name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `e_eintraege`
--

LOCK TABLES `e_eintraege` WRITE;
/*!40000 ALTER TABLE `e_eintraege` DISABLE KEYS */;
/*!40000 ALTER TABLE `e_eintraege` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `es_einstellungen`
--

DROP TABLE IF EXISTS `es_einstellungen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `es_einstellungen` (
  `es_propolis` tinyint(1) DEFAULT 1,
  `es_wirrbau` tinyint(1) DEFAULT 1,
  `es_stetigkeit` tinyint(1) DEFAULT 1,
  `es_sanftmut` tinyint(1) DEFAULT 1,
  `es_honigw` tinyint(1) DEFAULT 1,
  `es_brutw` tinyint(1) DEFAULT 1,
  `es_pollenw` tinyint(1) DEFAULT 1,
  `es_drohnenw` tinyint(1) DEFAULT 1,
  `es_leerw` tinyint(1) DEFAULT 1,
  `es_mittelw` tinyint(1) DEFAULT 1,
  `es_drohnenbn` tinyint(1) DEFAULT 1,
  `es_bannwaben` tinyint(1) DEFAULT 1,
  `es_milchsaeure` tinyint(1) DEFAULT 1,
  `es_streifen` tinyint(1) DEFAULT 1,
  `es_waermebehandlung` tinyint(1) DEFAULT 1,
  `es_ablegerbildung` tinyint(1) DEFAULT 1,
  `es_brutwabenentsorgung` tinyint(1) DEFAULT 1,
  `es_phosphorsaeureester` tinyint(1) DEFAULT 1,
  `es_pyrethroide` tinyint(1) DEFAULT 1,
  `es_metarhizium-pilz` tinyint(1) DEFAULT 1,
  `es_buecherskorpion` tinyint(1) DEFAULT 1,
  `es_aetherische-oele` tinyint(1) DEFAULT 1,
  `es_ameisensaeure_schwammtuch` tinyint(1) DEFAULT 1,
  `es_ameisensaeure_universal_verdunster` tinyint(1) DEFAULT 1,
  `es_ameisensaeure_liebig_verdunster` tinyint(1) DEFAULT 1,
  `es_ameisensaeure_nassenheider_verdunster` tinyint(1) DEFAULT 1,
  `es_oxalsaeure_traeufeln` tinyint(1) DEFAULT 1,
  `es_oxalsaeure_spruehen` tinyint(1) DEFAULT 1,
  `es_oxalsaeure_e-verdampfer_mit_thermostat` tinyint(1) DEFAULT 1,
  `es_oxalsaeure_e-verdampfer_ohne_thermostat` tinyint(1) DEFAULT 1,
  `es_oxalsaeure_gas_verdampfer` tinyint(1) DEFAULT 1,
  `es_oxalsaeure_vernebler` tinyint(1) DEFAULT 1,
  `es_user` varchar(50) NOT NULL,
  PRIMARY KEY (`es_user`),
  CONSTRAINT `fk_es_einstellungen_u_user1` FOREIGN KEY (`es_user`) REFERENCES `u_user` (`u_name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `es_einstellungen`
--

LOCK TABLES `es_einstellungen` WRITE;
/*!40000 ALTER TABLE `es_einstellungen` DISABLE KEYS */;
/*!40000 ALTER TABLE `es_einstellungen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `k_koeniginnen`
--

DROP TABLE IF EXISTS `k_koeniginnen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `k_koeniginnen` (
  `k_id` int(11) NOT NULL,
  `k_status` tinyint(1) DEFAULT 1,
  `k_name` varchar(100) NOT NULL,
  `k_jahrgang` int(11) NOT NULL,
  `k_infos` varchar(100) DEFAULT NULL,
  `k_user` varchar(50) NOT NULL,
  PRIMARY KEY (`k_id`),
  KEY `fk_k_koeniginnen_u_user1_idx` (`k_user`),
  CONSTRAINT `fk_k_koeniginnen_u_user1` FOREIGN KEY (`k_user`) REFERENCES `u_user` (`u_name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `k_koeniginnen`
--

LOCK TABLES `k_koeniginnen` WRITE;
/*!40000 ALTER TABLE `k_koeniginnen` DISABLE KEYS */;
/*!40000 ALTER TABLE `k_koeniginnen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `s_stoecke`
--

DROP TABLE IF EXISTS `s_stoecke`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_stoecke` (
  `s_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_status` tinyint(1) DEFAULT 1,
  `s_name` varchar(100) NOT NULL,
  `s_infos` varchar(100) DEFAULT NULL,
  `s_koenigin` int(11) DEFAULT NULL,
  `s_standorte` int(11) NOT NULL,
  PRIMARY KEY (`s_id`),
  KEY `fk_s_stoecke_st_standorte1_idx` (`s_standorte`),
  KEY `fk_s_stoecke_k_koeniginnen1_idx` (`s_koenigin`),
  CONSTRAINT `fk_s_stoecke_k_koeniginnen1` FOREIGN KEY (`s_koenigin`) REFERENCES `k_koeniginnen` (`k_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_s_stoecke_st_standorte1` FOREIGN KEY (`s_standorte`) REFERENCES `st_standorte` (`st_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `s_stoecke`
--

LOCK TABLES `s_stoecke` WRITE;
/*!40000 ALTER TABLE `s_stoecke` DISABLE KEYS */;
/*!40000 ALTER TABLE `s_stoecke` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `st_standorte`
--

DROP TABLE IF EXISTS `st_standorte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `st_standorte` (
  `st_id` int(11) NOT NULL AUTO_INCREMENT,
  `st_status` tinyint(1) DEFAULT 1,
  `st_name` varchar(100) NOT NULL,
  `st_adresse` varchar(100) DEFAULT NULL,
  `st_infos` varchar(100) DEFAULT NULL,
  `st_user` varchar(50) NOT NULL,
  PRIMARY KEY (`st_id`),
  KEY `fk_st_standorte_u_user1_idx` (`st_user`),
  CONSTRAINT `fk_st_standorte_u_user1` FOREIGN KEY (`st_user`) REFERENCES `u_user` (`u_name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `st_standorte`
--

LOCK TABLES `st_standorte` WRITE;
/*!40000 ALTER TABLE `st_standorte` DISABLE KEYS */;
/*!40000 ALTER TABLE `st_standorte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_token`
--

DROP TABLE IF EXISTS `t_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_token` (
  `t_id` varchar(100) NOT NULL,
  `t_email` varchar(100) NOT NULL,
  `t_datetime` date NOT NULL,
  PRIMARY KEY (`t_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_token`
--

LOCK TABLES `t_token` WRITE;
/*!40000 ALTER TABLE `t_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `u_user`
--

DROP TABLE IF EXISTS `u_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `u_user` (
  `u_name` varchar(50) NOT NULL,
  `u_email` varchar(100) NOT NULL,
  `u_telefon` varchar(25) DEFAULT NULL,
  `u_hash` varchar(100) NOT NULL,
  `u_salt` varchar(100) NOT NULL,
  `u_bild` int(11) DEFAULT NULL,
  `u_status` enum('aktiv','nicht aktiv','Im Wartezustand') DEFAULT NULL,
  PRIMARY KEY (`u_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `u_user`
--

LOCK TABLES `u_user` WRITE;
/*!40000 ALTER TABLE `u_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `u_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-04  9:49:26
