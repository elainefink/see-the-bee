-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema seethebee
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema seethebee
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `seethebee` DEFAULT CHARACTER SET utf8 ;
USE `seethebee` ;

-- -----------------------------------------------------
-- Table `seethebee`.`u_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `seethebee`.`u_user` (
  `u_name` Varchar(50) NOT NULL ,
  `u_email` VARCHAR(100) NOT NULL,
  `u_telefon` VARCHAR(25) NULL,
  `u_hash` VARCHAR(100) NOT NULL,
  `u_salt` VARCHAR(100) NOT NULL,
  `u_bild` INT NULL,
  `u_status` ENUM('aktiv', 'nicht aktiv', 'Im Wartezustand') NOT NULL,
  PRIMARY KEY (`u_name`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `seethebee`.`k_koeniginnen`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `seethebee`.`k_koeniginnen` (
  `k_id` INT NOT NULL AUTO_INCREMENT,
  `k_status` TINYINT(1) NOT NULL DEFAULT 1,
  `k_name` VARCHAR(100) NOT NULL,
  `k_jahrgang` INT NOT NULL,
  `k_infos` VARCHAR(100) NULL,
  `k_user` Varchar(50) NOT NULL,
  PRIMARY KEY (`k_id`),
  INDEX `fk_k_koeniginnen_u_user1_idx` (`k_user` ASC) ,
  CONSTRAINT `fk_k_koeniginnen_u_user1`
    FOREIGN KEY (`k_user`)
    REFERENCES `seethebee`.`u_user` (`u_name`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `seethebee`.`st_standorte`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `seethebee`.`st_standorte` (
  `st_id` INT NOT NULL AUTO_INCREMENT,
  `st_status` TINYINT(1) NOT NULL DEFAULT 1,
  `st_name` VARCHAR(100) NOT NULL,
  `st_adresse` VARCHAR(100) NULL,
  `st_infos` VARCHAR(100) NULL,
  `st_user` Varchar(50),
  PRIMARY KEY (`st_id`),
  INDEX `fk_st_standorte_u_user1_idx` (`st_user` ASC) ,
  CONSTRAINT `fk_st_standorte_u_user1`
    FOREIGN KEY (`st_user`)
    REFERENCES `seethebee`.`u_user` (`u_name`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `seethebee`.`s_stoecke`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `seethebee`.`s_stoecke` (
  `s_id` INT NOT NULL AUTO_INCREMENT,
  `s_status` TINYINT(1) NOT NULL DEFAULT 1,
  `s_name` VARCHAR(100) NOT NULL,
  `s_infos` VARCHAR(100) NULL,
  `s_koenigin` INT ,
  `s_standorte` INT ,
  PRIMARY KEY (`s_id`),
  INDEX `fk_s_stoecke_st_standorte1_idx` (`s_standorte` ASC),
  INDEX `fk_s_stoecke_k_koeniginnen1_idx` (`s_koenigin` ASC),
  CONSTRAINT `fk_s_stoecke_st_standorte1`
    FOREIGN KEY (`s_standorte`)
    REFERENCES `seethebee`.`st_standorte` (`st_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_s_stoecke_k_koeniginnen1`
    FOREIGN KEY (`s_koenigin`)
    REFERENCES `seethebee`.`k_koeniginnen` (`k_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `seethebee`.`e_eintraege`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `seethebee`.`e_eintraege` (
  `e_id` INT NOT NULL AUTO_INCREMENT,
  `e_date` DATE NOT NULL,
  `e_honigw` INT NULL,
  `e_brutw` INT NULL,
  `e_pollenw` INT NULL,
  `e_drohnenw` INT NULL,
  `e_leerw` INT NULL,
  `e_mittelw` INT NULL,
  `e_koenigin` TINYINT NULL,
  `e_propolis` ENUM('1', '2', '3', '4') NULL,
  `e_wirrbau` ENUM('1', '2', '3', '4') NULL,
  `e_stetigkeit` ENUM('1', '2', '3', '4') NULL,
  `e_sanftmut` ENUM('1', '2', '3', '4') NULL,
  `e_weiselz` INT NULL,
  `e_varroa` INT NULL,
  `e_infos` VARCHAR(100) NULL,
  `e_stoecke` INT,
  `e_von` VARCHAR(45) NULL,
  `e_bis` VARCHAR(45) NULL,
  `e_user` VARCHAR(50),
  `e_standorte` INT,
  PRIMARY KEY (`e_id`),
  INDEX `fk_e_eintraege_s_stoecke1_idx` (`e_stoecke` ASC) ,
  INDEX `fk_e_eintraege_st_standorte1_idx` (`e_standorte` ASC) ,
  INDEX `fk_e_eintraege_u_user1_idx` (`e_user` ASC) ,
  CONSTRAINT `fk_e_eintraege_s_stoecke1`
    FOREIGN KEY (`e_stoecke`)
    REFERENCES `seethebee`.`s_stoecke` (`s_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_e_eintraege_st_standorte1`
    FOREIGN KEY (`e_standorte`)
    REFERENCES `seethebee`.`st_standorte` (`st_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_e_eintraege_u_user1`
    FOREIGN KEY (`e_user`)
    REFERENCES `seethebee`.`u_user` (`u_name`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `seethebee`.`b_behandlungen`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `seethebee`.`b_behandlungen` (
  `b_id` INT NOT NULL AUTO_INCREMENT,
  `b_datevon` DATE NOT NULL,
  `b_behandlung` ENUM('Wärmebehandlung', 'Entnahme von Drohnenbrut', 'Bannwabenverfahren', 'Ablegerbildung', 'Entsorgen der Brutwaben', 'organische Phosphorsäureester', 'Pyrethroide', 'Metarhizium-Pilz', 'Bücherskorpion', 'Milchsäure sprühen', 'ätherische Öle', 'Ameisensäure Schwammtuch', 'Ameisensäure Universal-Verdunster', 'Ameisensäure Liebig-Verdunster', 'Ameisensäure Nassenheider-Verdunster', 'Oxalsäure träufeln', 'Oxalsäure sprühen', 'Oxalsäure E-Verdampfer mit Thermostat', 'Oxalsäure E-Verdampfer ohne Thermostat', 'Oxalsäure Gas-Verdampfer', 'Oxalsäure Vernebler', 'natürliche Evolution (ohne Fremdeinwirkung)') NOT NULL,
  `b_infos` VARCHAR(100) NULL,
  `b_stoecke` INT NOT NULL,
  `b_datebis` DATE NULL,
  `b_von` VARCHAR(45) NULL,
  `b_bis` VARCHAR(45) NULL,
  `b_user` VARCHAR(50) NOT NULL,
  `b_standorte` INT NOT NULL,
  PRIMARY KEY (`b_id`),
  INDEX `fk_b_behandlungen_s_stoecke1_idx` (`b_stoecke` ASC) ,
  INDEX `fk_b_behandlungen_u_user1_idx` (`b_user` ASC) ,
  INDEX `fk_b_behandlungen_st_standorte1_idx` (`b_standorte` ASC) ,
  CONSTRAINT `fk_b_behandlungen_s_stoecke1`
    FOREIGN KEY (`b_stoecke`)
    REFERENCES `seethebee`.`s_stoecke` (`s_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_b_behandlungen_u_user1`
    FOREIGN KEY (`b_user`)
    REFERENCES `seethebee`.`u_user` (`u_name`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_b_behandlungen_st_standorte1`
    FOREIGN KEY (`b_standorte`)
    REFERENCES `seethebee`.`st_standorte` (`st_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `seethebee`.`es_einstellungen`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `seethebee`.`es_einstellungen` (
  `es_propolis` TINYINT(1) NOT NULL DEFAULT 1,
  `es_wirrbau` TINYINT(1) NOT NULL DEFAULT 1,
  `es_stetigkeit` TINYINT(1) NOT NULL DEFAULT 1,
  `es_sanftmut` TINYINT(1) NOT NULL DEFAULT 1,
  `es_honigw` TINYINT(1) NOT NULL DEFAULT 1,
  `es_brutw` TINYINT(1) NOT NULL DEFAULT 1,
  `es_pollenw` TINYINT(1) NOT NULL DEFAULT 1,
  `es_drohnenw` TINYINT(1) NOT NULL DEFAULT 1,
  `es_leerw` TINYINT(1) NOT NULL DEFAULT 1,
  `es_mittelw` TINYINT(1) NOT NULL DEFAULT 1,
  `es_drohnenbn` TINYINT(1) NOT NULL DEFAULT 1,
  `es_bannwaben` TINYINT(1) NOT NULL DEFAULT 1,
  `es_milchsaeure` TINYINT(1) NOT NULL DEFAULT 1,
  `es_waermebehandlung` TINYINT(1) NOT NULL DEFAULT 1,
  `es_ablegerbildung` TINYINT(1) NOT NULL DEFAULT 1,
  `es_brutwabenentsorgung` TINYINT(1) NOT NULL DEFAULT 1,
  `es_phosphorsaeureester` TINYINT(1) NOT NULL DEFAULT 1,
  `es_pyrethroide` TINYINT(1) NOT NULL DEFAULT 1,
  `es_metarhizium-pilz` TINYINT(1) NOT NULL DEFAULT 1,
  `es_buecherskorpion` TINYINT(1) NOT NULL DEFAULT 1,
  `es_aetherische-oele` TINYINT(1) NOT NULL DEFAULT 1,
  `es_ameisensaeure_schwammtuch` TINYINT(1) NOT NULL DEFAULT 1,
  `es_ameisensaeure_universal_verdunster` TINYINT(1) NOT NULL DEFAULT 1,
  `es_ameisensaeure_liebig_verdunster` TINYINT(1) NOT NULL DEFAULT 1,
  `es_ameisensaeure_nassenheider_verdunster` TINYINT(1) NOT NULL DEFAULT 1,
  `es_oxalsaeure_traeufeln` TINYINT(1) NOT NULL DEFAULT 1,
  `es_oxalsaeure_spruehen` TINYINT(1) NOT NULL DEFAULT 1,
  `es_oxalsaeure_e-verdampfer_mit_thermostat` TINYINT(1) NOT NULL DEFAULT 1,
  `es_oxalsaeure_e-verdampfer_ohne_thermostat` TINYINT(1) NOT NULL DEFAULT 1,
  `es_oxalsaeure_gas_verdampfer` TINYINT(1) NOT NULL DEFAULT 1,
  `es_oxalsaeure_vernebler` TINYINT(1) NOT NULL DEFAULT 1,
  `es_user` VARCHAR(50) NOT NULL, 
  PRIMARY KEY (`es_user`),
  CONSTRAINT `fk_es_einstellungen_s_stoecke`
    FOREIGN KEY (`es_user`)
    REFERENCES `seethebee`.`u_user` (`u_name`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `seethebee`.`t_token`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `seethebee`.`t_token` (
  `t_id` VARCHAR(100) NOT NULL,
  `t_email` VARCHAR(100) NOT NULL,
  `t_datetime` DATETime NOT NULL,
  PRIMARY KEY (`t_id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;