import webbrowser
from flask import Flask, render_template, request, Response, jsonify
from flask_cors import CORS
from flaskext.mysql import MySQL
import json
import datetime

app = Flask(__name__)
CORS(app)
mysql = MySQL()
app.config['MYSQL_DATABASE_USER'] = 'k165810_root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'IntelliBee4444!'
app.config['MYSQL_DATABASE_DB'] = 'k165810_SeeTheBee'
app.config['MYSQL_DATABASE_HOST'] = 'db.seethebee.co.at'
mysql.init_app(app)


def home():
    return render_template('index.html')


@app.route('/login')
def login():
    return render_template('index.html')


"""
@app.route('/home')
def home():
    return render_template('home.html')
"""


# --------------------------------------------------------------------------------------User----------------------------------------------------

@app.route('/users', methods=['GET'])
def get_users():
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM u_user")
    users = []
    for i, row in enumerate(cursor.fetchall()):
        temp = {}
        for j, value in enumerate(row):
            if isinstance(value, datetime.date):
                temp[cursor.description[j][0]] = value.strftime("%Y-%m-%d")
            else:
                temp[cursor.description[j][0]] = value
        users.append(temp)
    cursor.close()
    json_object = json.dumps(users)
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/user/<u_id>', methods=['GET'])
def get_user(u_id):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM u_user WHERE u_name= %s", u_id)
    row = cursor.fetchone()
    if row is None:
        json_dict = {
            'statusCode': 404,
            'error': 'Not Found'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=404, mimetype='application/json')
    else:
        user_dict = {}
        for i, value in enumerate(row):
            if isinstance(value, datetime.date):
                user_dict[cursor.description[i][0]] = value.strftime("%Y-%m-%d")
            else:
                user_dict[cursor.description[i][0]] = value
        json_object = json.dumps(user_dict)
        return Response(json_object, status=200, mimetype='application/json')


@app.route('/user', methods=['POST'])
def add_user():
    content = json.loads(request.data)
    sqlcommand = "INSERT INTO u_user(`u_name`,`u_email`,`u_telefon`, `u_hash`, `u_salt`, `u_bild`,`u_status`) VALUES ('%s','%s','%s','%s','%s','%s','%s');" % (
        content["u_name"], content["u_email"], content["u_telefon"], content["u_hash"], content["u_salt"],
        content["u_bild"], 'aktiv')
    cursor = mysql.connect().cursor()
    cursor.execute(sqlcommand)
    cursor.connection.commit()
    json_object = json.dumps({'success': True, 'new_id': cursor.lastrowid})
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/user/<u_id>', methods=['DELETE'])
def delete_user(u_id):
    cursor = mysql.connect().cursor()
    cursor.execute("DELETE FROM u_user WHERE u_name = %s", u_id)
    cursor.connection.commit()
    deleted_row_count = cursor.rowcount
    cursor.close()
    if deleted_row_count == 1:
        json_dict = {
            'success': True,
            'message': 'User with id ' + u_id + ' deleted'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=200, mimetype='application/json')
    elif deleted_row_count == 0:
        json_dict = {
            'success': False,
            'message': 'There is no user with id ' + u_id
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=404, mimetype='application/json')
    else:
        json_dict = {
            'success': False,
            'message': 'More than one user is affected'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=400, mimetype='application/json')


@app.route('/user/<u_id>', methods=['PUT'])
def action_user(u_id):
    content = json.loads(request.data)
    cur = mysql.connect().cursor()
    cur.execute(
        "UPDATE u_user SET u_name = %s,u_email = %s, u_telefon = %s, u_hash = %s, u_salt = %s, u_bild = %s  WHERE u_name = %s",
        (content["u_name"], content["u_email"], content["u_telefon"], content["u_hash"], content["u_salt"],
         content["u_bild"], u_id))
    cur.connection.commit()
    json_dict = {
        'success': True,
        'message': 'User with id ' + u_id + ' updated'
    }
    json_object = json.dumps(json_dict)
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/user/deactivate/<u_id>', methods=['PUT'])
def deactivate_user(u_id):
    cur = mysql.connect().cursor()
    cur.execute("UPDATE u_user SET u_status = 0 WHERE u_name = %s", u_id)
    cur.connection.commit()
    json_dict = {
        'success': True,
        'message': 'User with id ' + u_id + 'deactivated'
    }
    json_object = json.dumps(json_dict)
    return Response(json_object, status=200, mimetype='application/json')


# --------------------------------------------------------------------------------------Koenigin----------------------------------------------------

@app.route('/queens/user/<u_name>', methods=['GET'])
def get_queens(u_name):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM k_koeniginnen INNER JOIN u_user on k_user = u_name WHERE k_user = %s", u_name)
    queens = []
    for i, row in enumerate(cursor.fetchall()):
        temp = {}
        for j, value in enumerate(row):
            if isinstance(value, datetime.date):
                temp[cursor.description[j][0]] = value.strftime("%Y-%m-%d")
            else:
                temp[cursor.description[j][0]] = value
        queens.append(temp)
    cursor.close()
    json_object = json.dumps(queens)
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/queen/stock/<s_koenigin>', methods=['GET'])
def get_stock_queen(s_koenigin):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM k_koeniginnen INNER JOIN s_stoecke on k_id = s_koenigin WHERE k_id = %s", s_koenigin)
    row = cursor.fetchone()
    if row is None:
        json_dict = {
            'statusCode': 404,
            'error': 'Not Found'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=404, mimetype='application/json')
    else:
        queen_dict = {}
        for i, value in enumerate(row):
            if isinstance(value, datetime.date):
                queen_dict[cursor.description[i][0]] = value.strftime("%Y-%m-%d")
            else:
                queen_dict[cursor.description[i][0]] = value
        json_object = json.dumps(queen_dict)
        return Response(json_object, status=200, mimetype='application/json')


@app.route('/queen/0/stock/<s_koenigin>', methods=['GET'])
def get_stock_without_queen(s_koenigin):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM k_koeniginnen INNER JOIN s_stoecke on k_id = s_koenigin WHERE k_id = 0", s_koenigin)
    row = cursor.fetchone()
    if row is None:
        json_dict = {
            'statusCode': 404,
            'error': 'Not Found'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=404, mimetype='application/json')
    else:
        queen_dict = {}
        for i, value in enumerate(row):
            if isinstance(value, datetime.date):
                queen_dict[cursor.description[i][0]] = value.strftime("%Y-%m-%d")
            else:
                queen_dict[cursor.description[i][0]] = value
        json_object = json.dumps(queen_dict)
        return Response(json_object, status=200, mimetype='application/json')


@app.route('/queen/<k_id>', methods=['GET'])
def get_queen(k_id):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM k_koeniginnen WHERE k_id= %s", k_id)
    row = cursor.fetchone()
    if row is None:
        json_dict = {
            'statusCode': 404,
            'error': 'Not Found'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=404, mimetype='application/json')
    else:
        queen_dict = {}
        for i, value in enumerate(row):
            if isinstance(value, datetime.date):
                queen_dict[cursor.description[i][0]] = value.strftime("%Y-%m-%d")
            else:
                queen_dict[cursor.description[i][0]] = value
        json_object = json.dumps(queen_dict)
        return Response(json_object, status=200, mimetype='application/json')


@app.route('/queen/<u_id>', methods=['POST'])
def add_queen(u_id):
    content = json.loads(request.data)
    sqlcommand = "INSERT INTO k_koeniginnen(`k_status`,`k_name`, `k_jahrgang`, `k_infos`, `k_user`) VALUES ('%s','%s','%s','%s','%s');" % (
        content["k_status"], content["k_name"], content["k_jahrgang"], content["k_infos"], u_id)
    cursor = mysql.connect().cursor()
    cursor.execute(sqlcommand)
    cursor.connection.commit()
    json_object = json.dumps({'success': True, 'new_id': cursor.lastrowid})
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/queen/<k_id>', methods=['DELETE'])
def delete_queen(k_id):
    cursor = mysql.connect().cursor()
    cursor.execute("DELETE FROM k_koeniginnen WHERE k_id = %s", k_id)
    cursor.connection.commit()
    deleted_row_count = cursor.rowcount
    cursor.close()
    if deleted_row_count == 1:
        json_dict = {
            'success': True,
            'message': 'Queen with id ' + k_id + ' deleted'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=200, mimetype='application/json')
    elif deleted_row_count == 0:
        json_dict = {
            'success': False,
            'message': 'There is no queen with id ' + k_id
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=404, mimetype='application/json')
    else:
        json_dict = {
            'success': False,
            'message': 'More than one queen is affected'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=400, mimetype='application/json')


@app.route('/queen/deactivate/<k_id>', methods=['PUT'])
def deactivate_queen(k_id):
    cur = mysql.connect().cursor()
    cur.execute("UPDATE k_koeniginnen SET k_status = 0 WHERE k_id = %s", k_id)
    cur.connection.commit()
    json_dict = {
        'success': True,
        'message': 'queen with id ' + k_id + 'deactivated'
    }
    json_object = json.dumps(json_dict)
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/queen/<k_id>', methods=['PUT'])
def action_queen(k_id):
    content = json.loads(request.data)
    cur = mysql.connect().cursor()
    cur.execute(
        "UPDATE k_koeniginnen SET k_status = %s, k_name = %s, k_jahrgang = %s, k_infos = %s, k_user = %s WHERE k_id = %s",
        (content["k_status"], content["k_name"], content["k_jahrgang"], content["k_infos"],
         content["k_user"], k_id))
    cur.connection.commit()
    json_dict = {
        'success': True,
        'message': 'Queen with id ' + k_id + ' updated'
    }
    json_object = json.dumps(json_dict)
    return Response(json_object, status=200, mimetype='application/json')


# --------------------------------------------------------------------------------------Standorte----------------------------------------------------

@app.route('/orte/user/<u_name>', methods=['GET'])
def get_user_orte(u_name):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM st_standorte INNER JOIN u_user on st_user = u_name WHERE st_user = %s", u_name)
    standorte = []
    for i, row in enumerate(cursor.fetchall()):
        temp = {}
        for j, value in enumerate(row):
            if isinstance(value, datetime.date):
                temp[cursor.description[j][0]] = value.strftime("%Y-%m-%d")
            else:
                temp[cursor.description[j][0]] = value
        standorte.append(temp)
    cursor.close()
    json_object = json.dumps(standorte)
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/ort/<st_id>', methods=['GET'])
def get_ort(st_id):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM st_standorte WHERE st_id= %s", st_id)
    row = cursor.fetchone()
    if row is None:
        json_dict = {
            'statusCode': 404,
            'error': 'Not Found'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=404, mimetype='application/json')
    else:
        ort_dict = {}
        for i, value in enumerate(row):
            if isinstance(value, datetime.date):
                ort_dict[cursor.description[i][0]] = value.strftime("%Y-%m-%d")
            else:
                ort_dict[cursor.description[i][0]] = value
        json_object = json.dumps(ort_dict)
        return Response(json_object, status=200, mimetype='application/json')


@app.route('/ort/<u_name>', methods=['POST'])
def add_ort(u_name):
    content = json.loads(request.data)
    sqlcommand = "INSERT INTO st_standorte(`st_status`,`st_name`, `st_adresse`, `st_infos`, `st_user`) VALUES ('%s','%s','%s','%s','%s');" % (
        content["st_status"], content["st_name"], content["st_adresse"], content["st_infos"], u_name)
    cursor = mysql.connect().cursor()
    cursor.execute(sqlcommand)
    cursor.connection.commit()
    json_object = json.dumps({'success': True, 'new_id': cursor.lastrowid})
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/ort/<st_id>', methods=['DELETE'])
def delete_ort(st_id):
    cursor = mysql.connect().cursor()
    cursor.execute("DELETE FROM st_standorte WHERE st_id = %s", st_id)
    cursor.connection.commit()
    deleted_row_count = cursor.rowcount
    cursor.close()
    if deleted_row_count == 1:
        json_dict = {
            'success': True,
            'message': 'Location with id ' + st_id + ' deleted'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=200, mimetype='application/json')
    elif deleted_row_count == 0:
        json_dict = {
            'success': False,
            'message': 'There is no location with id ' + st_id
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=404, mimetype='application/json')
    else:
        json_dict = {
            'success': False,
            'message': 'More than one location is affected'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=400, mimetype='application/json')


@app.route('/ort/deactivate/<st_id>', methods=['PUT'])
def deactivate_ort(st_id):
    cur = mysql.connect().cursor()
    cur.execute("UPDATE st_standorte SET st_status = 0 WHERE st_id = %s", st_id)
    cur.connection.commit()
    json_dict = {
        'success': True,
        'message': 'Location with id ' + st_id + 'deactivated'
    }
    json_object = json.dumps(json_dict)
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/ort/<st_id>', methods=['PUT'])
def action_ort(st_id):
    content = json.loads(request.data)
    cur = mysql.connect().cursor()
    cur.execute(
        "UPDATE st_standorte SET st_status = %s, st_name = %s, st_adresse = %s, st_infos = %s, st_user = %s WHERE st_id = %s",
        (content["st_status"], content["st_name"], content["st_adresse"], content["st_infos"],
         content["st_user"], st_id))
    cur.connection.commit()
    json_dict = {
        'success': True,
        'message': 'Location with id ' + st_id + ' updated'
    }
    json_object = json.dumps(json_dict)
    return Response(json_object, status=200, mimetype='application/json')


# --------------------------------------------------------------------------------------Stoecke----------------------------------------------------

@app.route('/stoecke/ort/<st_id>', methods=['GET'])
def get_ort_stoecke(st_id):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM s_stoecke INNER JOIN st_standorte on s_standorte = st_id WHERE s_standorte = %s",
                   st_id)
    stoecke = []
    for i, row in enumerate(cursor.fetchall()):
        temp = {}
        for j, value in enumerate(row):
            if isinstance(value, datetime.date):
                temp[cursor.description[j][0]] = value.strftime("%Y-%m-%d")
            else:
                temp[cursor.description[j][0]] = value
        stoecke.append(temp)
    cursor.close()
    json_object = json.dumps(stoecke)
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/stock/<s_id>', methods=['GET'])
def get_stock(s_id):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM s_stoecke WHERE s_id= %s", s_id)
    row = cursor.fetchone()
    if row is None:
        json_dict = {
            'statusCode': 404,
            'error': 'Not Found'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=404, mimetype='application/json')
    else:
        stock_dict = {}
        for i, value in enumerate(row):
            if isinstance(value, datetime.date):
                stock_dict[cursor.description[i][0]] = value.strftime("%Y-%m-%d")
            else:
                stock_dict[cursor.description[i][0]] = value
        json_object = json.dumps(stock_dict)
        return Response(json_object, status=200, mimetype='application/json')


@app.route('/stock/<st_id>', methods=['POST'])
def add_stock(st_id):
    content = json.loads(request.data)
    sqlcommand = "INSERT INTO s_stoecke(`s_status`,`s_name`, `s_koenigin`, `s_infos`, `s_standorte`) VALUES ('%s','%s','%s','%s','%s');" % (
        content["s_status"], content["s_name"], content["s_koenigin"], content["s_infos"], st_id)
    cursor = mysql.connect().cursor()
    cursor.execute(sqlcommand)
    cursor.connection.commit()
    json_object = json.dumps({'success': True, 'new_id': cursor.lastrowid})
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/stock/<s_id>', methods=['DELETE'])
def delete_stock(s_id):
    cursor = mysql.connect().cursor()
    cursor.execute("DELETE FROM s_stoecke WHERE s_id = %s", s_id)
    cursor.connection.commit()
    deleted_row_count = cursor.rowcount
    cursor.close()
    if deleted_row_count == 1:
        json_dict = {
            'success': True,
            'message': 'Beehive with id ' + s_id + ' deleted'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=200, mimetype='application/json')
    elif deleted_row_count == 0:
        json_dict = {
            'success': False,
            'message': 'There is no beehive with id ' + s_id
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=404, mimetype='application/json')
    else:
        json_dict = {
            'success': False,
            'message': 'More than one beehive is affected'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=400, mimetype='application/json')


@app.route('/stock/deactivate/<s_id>', methods=['PUT'])
def deactivate_stock(s_id):
    cur = mysql.connect().cursor()
    cur.execute("UPDATE s_stoecke SET s_status = 0 WHERE s_id = %s", s_id)
    cur.connection.commit()
    json_dict = {
        'success': True,
        'message': 'Beehive with id ' + s_id + 'deactivated'
    }
    json_object = json.dumps(json_dict)
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/stock/<s_id>', methods=['PUT'])
def action_stock(s_id):
    content = json.loads(request.data)
    cur = mysql.connect().cursor()
    cur.execute(
        "UPDATE s_stoecke SET s_status = %s, s_name = %s, s_koenigin = %s, s_infos = %s, s_standorte = %s WHERE s_id = %s",
        (content["s_status"], content["s_name"], content["s_koenigin"], content["s_infos"], content["s_standorte"],
         s_id))
    cur.connection.commit()
    json_dict = {
        'success': True,
        'message': 'Beehive with id ' + s_id + ' updated'
    }
    json_object = json.dumps(json_dict)
    return Response(json_object, status=200, mimetype='application/json')


# --------------------------------------------------------------------------------------Einstellungen----------------------------------------------------

@app.route('/settings', methods=['GET'])
def get_settings():
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM es_einstellungen")
    settings = []
    for i, row in enumerate(cursor.fetchall()):
        temp = {}
        for j, value in enumerate(row):
            if isinstance(value, datetime.date):
                temp[cursor.description[j][0]] = value.strftime("%Y-%m-%d")
            else:
                temp[cursor.description[j][0]] = value
        settings.append(temp)
    cursor.close()
    json_object = json.dumps(settings)
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/setting/<es_user>', methods=['GET'])
def get_setting(es_user):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM es_einstellungen WHERE es_user= %s", es_user)
    row = cursor.fetchone()
    if row is None:
        json_dict = {
            'statusCode': 404,
            'error': 'Not Found'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=404, mimetype='application/json')
    else:
        setting_dict = {}
        for i, value in enumerate(row):
            if isinstance(value, datetime.date):
                setting_dict[cursor.description[i][0]] = value.strftime("%Y-%m-%d")
            else:
                setting_dict[cursor.description[i][0]] = value
        json_object = json.dumps(setting_dict)
        return Response(json_object, status=200, mimetype='application/json')


@app.route('/setting/<es_user>', methods=['PUT'])
def action_setting(es_user):
    content = json.loads(request.data)
    cur = mysql.connect().cursor()
    cur.execute(
        "UPDATE es_einstellungen SET es_propolis = %s, es_wirrbau = %s, es_stetigkeit = %s, es_sanftmut = %s, es_honigw = %s, es_brutw = %s, "
        "es_pollenw = %s, es_drohnenw = %s, es_leerw = %s, es_mittelw = %s, es_drohnenbn = %s, es_bannwaben = %s, es_milchsaeure = %s,"
        "es_waermebehandlung = %s, es_ablegerbildung = %s, es_brutwabenentsorgung = %s, es_phosphorsaeureester = %s, es_pyrethroide = %s, "
        "`es_metarhizium-pilz` = %s, es_buecherskorpion = %s, `es_aetherische-oele` = %s, es_ameisensaeure_schwammtuch = %s,"
        " es_ameisensaeure_universal_verdunster = %s, es_ameisensaeure_liebig_verdunster = %s, es_ameisensaeure_nassenheider_verdunster = %s, "
        "es_oxalsaeure_traeufeln = %s,es_oxalsaeure_spruehen = %s, `es_oxalsaeure_e-verdampfer_mit_thermostat` = %s, "
        "`es_oxalsaeure_e-verdampfer_ohne_thermostat` = %s, es_oxalsaeure_gas_verdampfer = %s, es_oxalsaeure_vernebler = %s "
        "WHERE es_user = %s",
        (content["prop"], content["wirr"], content["stetig"], content["sanft"], content["hw"], content["bw"],
         content["pw"], content["dw"], content["lw"], content["mw"], content["drohnenent"], content["bann"],
         content["milch"], content["waerme"], content["ableger"], content["brutent"], content["op"],
         content["py"], content["mp"], content["bs"], content["ao"], content["ass"], content["asu"],
         content["asl"], content["asn"], content["ost"], content["oss"], content["oset"], content["oso"],
         content["osg"], content["osn"], es_user))
    cur.connection.commit()
    json_dict = {
        'success': True,
        'message': 'Setting with id ' + es_user + ' updated'
    }
    json_object = json.dumps(json_dict)
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/setting/<u_id>', methods=['POST'])
def add_setting(u_id):
    content = json.loads(request.data)
    cursor = mysql.connect().cursor()
    cursor.execute(
        "INSERT INTO es_einstellungen(`es_propolis`,`es_wirrbau`, `es_stetigkeit`, `es_sanftmut`, `es_honigw`,`es_brutw`,`es_pollenw`, "
        "`es_drohnenw`, `es_leerw`, `es_mittelw`, `es_drohnenbn`, `es_bannwaben`, `es_milchsaeure`, `es_waermebehandlung`,`es_ablegerbildung`,"
        " `es_brutwabenentsorgung`, `es_phosphorsaeureester`, `es_pyrethroide`, `es_metarhizium-pilz`,`es_buecherskorpion`, "
        "`es_aetherische-oele`, `es_ameisensaeure_schwammtuch`,`es_ameisensaeure_universal_verdunster`, `es_ameisensaeure_liebig_verdunster`, "
        "`es_ameisensaeure_nassenheider_verdunster`, `es_oxalsaeure_traeufeln`,`es_oxalsaeure_spruehen`,`es_oxalsaeure_e-verdampfer_mit_thermostat`,"
        " `es_oxalsaeure_e-verdampfer_ohne_thermostat`, `es_oxalsaeure_gas_verdampfer`, `es_oxalsaeure_vernebler`, `es_user`)"
        "VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s', '%s','%s','%s','%s','%s', '%s','%s','%s','%s','%s', '%s', '%s');" %
        (content["prop"], content["wirr"], content["stetig"], content["sanft"], content["hw"], content["bw"],
         content["pw"], content["dw"],
         content["lw"], content["mw"], content["drohnenent"], content["bann"], content["milch"], content["waerme"],
         content["ableger"], content["brutent"],
         content["op"], content["py"], content["mp"], content["bs"], content["ao"], content["ass"], content["asu"],
         content["asl"],
         content["asn"], content["ost"], content["oss"], content["oset"], content["oso"], content["osg"],
         content["osn"], u_id)
    )
    cursor.connection.commit()
    json_object = json.dumps({'success': True, 'new_id': cursor.lastrowid})
    return Response(json_object, status=200, mimetype='application/json')


# --------------------------------------------------------------------------------------Eintraege----------------------------------------------------

@app.route('/entrys/user/<u_name>', methods=['GET'])
def get_user_entrys(u_name):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM e_eintraege INNER JOIN u_user on e_user = u_name WHERE e_user = %s", u_name)
    entrys = []
    for i, row in enumerate(cursor.fetchall()):
        temp = {}
        for j, value in enumerate(row):
            if isinstance(value, datetime.date):
                temp[cursor.description[j][0]] = value.strftime("%Y-%m-%d")
            else:
                temp[cursor.description[j][0]] = value
        entrys.append(temp)
    cursor.close()
    json_object = json.dumps(entrys)
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/entrys/ort/<st_id>', methods=['GET'])
def get_ort_entrys(st_id):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM e_eintraege INNER JOIN st_standorte on e_standorte = st_id WHERE e_standorte = %s",
                   st_id)
    entrys = []
    for i, row in enumerate(cursor.fetchall()):
        temp = {}
        for j, value in enumerate(row):
            if isinstance(value, datetime.date):
                temp[cursor.description[j][0]] = value.strftime("%Y-%m-%d")
            else:
                temp[cursor.description[j][0]] = value
        entrys.append(temp)
    cursor.close()
    json_object = json.dumps(entrys)
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/entrys/stock/<s_id>', methods=['GET'])
def get_beehive_entrys(s_id):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM e_eintraege INNER JOIN s_stoecke on e_stoecke = s_id WHERE e_stoecke = %s", s_id)
    entrys = []
    for i, row in enumerate(cursor.fetchall()):
        temp = {}
        for j, value in enumerate(row):
            if isinstance(value, datetime.date):
                temp[cursor.description[j][0]] = value.strftime("%Y-%m-%d")
            else:
                temp[cursor.description[j][0]] = value
        entrys.append(temp)
    cursor.close()
    json_object = json.dumps(entrys)
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/entry/<e_id>', methods=['GET'])
def get_entry(e_id):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM e_eintraege WHERE e_id= %s", e_id)
    row = cursor.fetchone()
    if row is None:
        json_dict = {
            'statusCode': 404,
            'error': 'Not Found'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=404, mimetype='application/json')
    else:
        entry_dict = {}
        for i, value in enumerate(row):
            if isinstance(value, datetime.date):
                entry_dict[cursor.description[i][0]] = value.strftime("%Y-%m-%d")
            else:
                entry_dict[cursor.description[i][0]] = value
        json_object = json.dumps(entry_dict)
        return Response(json_object, status=200, mimetype='application/json')


@app.route('/entry/<s_id>', methods=['POST'])
def add_entry(s_id):
    content = json.loads(request.data)
    sqlcommand = "INSERT INTO e_eintraege(`e_date`, `e_honigw`, `e_brutw`,`e_pollenw`,`e_drohnenw`,`e_leerw`, `e_mittelw`, `e_koenigin` , `e_propolis`, `e_wirrbau`, `e_stetigkeit`, `e_sanftmut`, `e_weiselz`, `e_varroa`, `e_infos`, `e_stoecke`, `e_standorte`, `e_user`,`e_von`,`e_bis`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');" % (
        content["e_date"], content["e_honigw"], content["e_brutw"],
        content["e_pollenw"], content["e_drohnenw"], content["e_leerw"],
        content["e_mittelw"], content["e_koenigin"], content["e_propolis"],
        content["e_wirrbau"], content["e_stetigkeit"], content["e_sanftmut"],
        content["e_weiselz"], content["e_varroa"], content["e_infos"],
        s_id, content["e_standorte"], content["e_user"],
        content["e_von"], content["e_bis"])
    cursor = mysql.connect().cursor()
    cursor.execute(sqlcommand)
    cursor.connection.commit()
    json_object = json.dumps({'success': True, 'new_id': cursor.lastrowid})
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/entry/<e_id>', methods=['DELETE'])
def delete_entry(e_id):
    cursor = mysql.connect().cursor()
    cursor.execute("DELETE FROM e_eintraege WHERE e_id = %s", e_id)
    cursor.connection.commit()
    deleted_row_count = cursor.rowcount
    cursor.close()
    if deleted_row_count == 1:
        json_dict = {
            'success': True,
            'message': 'Entry with id ' + e_id + ' deleted'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=200, mimetype='application/json')
    elif deleted_row_count == 0:
        json_dict = {
            'success': False,
            'message': 'There is no entry with id ' + e_id
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=404, mimetype='application/json')
    else:
        json_dict = {
            'success': False,
            'message': 'More than one entry is affected'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=400, mimetype='application/json')


@app.route('/entry/<e_id>', methods=['PUT'])
def action_entry(e_id):
    content = json.loads(request.data)
    cur = mysql.connect().cursor()
    cur.execute(
        "UPDATE e_eintraege SET e_date = %s, e_honigw = %s, e_brutw = %s, e_pollenw = %s, e_drohnenw = %s, e_leerw = %s, e_mittelw = %s, e_koenigin = %s, e_propolis = %s, e_wirrbau = %s, e_stetigkeit = %s, e_sanftmut = %s, e_weiselz = %s, e_varroa = %s, e_infos = %s, e_stoecke = %s, e_standorte = %s, e_user = %s, e_von=%s, e_bis=%s WHERE e_id = %s",
        (content["e_date"], content["e_honigw"], content["e_brutw"], content["e_pollenw"], content["e_drohnenw"],
         content["e_leerw"], content["e_mittelw"], content["e_koenigin"], content["e_propolis"], content["e_wirrbau"],
         content["e_stetigkeit"], content["e_sanftmut"], content["e_weiselz"], content["e_varroa"], content["e_infos"],
         content["e_stoecke"], content["e_standorte"], content["e_user"], content["e_von"], content["e_bis"], e_id))
    cur.connection.commit()
    json_dict = {
        'success': True,
        'message': 'Entry with id ' + e_id + ' updated'
    }
    json_object = json.dumps(json_dict)
    return Response(json_object, status=200, mimetype='application/json')


# --------------------------------------------------------------------------------------Behandlungen----------------------------------------------------

@app.route('/treatments/user/<u_name>', methods=['GET'])
def get_user_treatments(u_name):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM b_behandlungen INNER JOIN u_user on b_user = u_name WHERE b_user = %s", u_name)
    treatments = []
    for i, row in enumerate(cursor.fetchall()):
        temp = {}
        for j, value in enumerate(row):
            if isinstance(value, datetime.date):
                temp[cursor.description[j][0]] = value.strftime("%Y-%m-%d")
            else:
                temp[cursor.description[j][0]] = value
        treatments.append(temp)
    cursor.close()
    json_object = json.dumps(treatments)
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/treatments/ort/<st_id>', methods=['GET'])
def get_ort_treatments(st_id):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM b_behandlungen INNER JOIN st_standorte on b_standorte = st_id WHERE b_standorte = %s",
                   st_id)
    treatments = []
    for i, row in enumerate(cursor.fetchall()):
        temp = {}
        for j, value in enumerate(row):
            if isinstance(value, datetime.date):
                temp[cursor.description[j][0]] = value.strftime("%Y-%m-%d")
            else:
                temp[cursor.description[j][0]] = value
        treatments.append(temp)
    cursor.close()
    json_object = json.dumps(treatments)
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/treatments/stock/<s_id>', methods=['GET'])
def get_beehive_treatments(s_id):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM b_behandlungen INNER JOIN s_stoecke on b_stoecke = s_id WHERE b_stoecke = %s", s_id)
    treatments = []
    for i, row in enumerate(cursor.fetchall()):
        temp = {}
        for j, value in enumerate(row):
            if isinstance(value, datetime.date):
                temp[cursor.description[j][0]] = value.strftime("%Y-%m-%d")
            else:
                temp[cursor.description[j][0]] = value
        treatments.append(temp)
    cursor.close()
    json_object = json.dumps(treatments)
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/treatment/<b_id>', methods=['GET'])
def get_treatment(b_id):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM b_behandlungen WHERE b_id= %s", b_id)
    row = cursor.fetchone()
    if row is None:
        json_dict = {
            'statusCode': 404,
            'error': 'Not Found'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=404, mimetype='application/json')
    else:
        treatment_dict = {}
        for i, value in enumerate(row):
            if isinstance(value, datetime.date):
                treatment_dict[cursor.description[i][0]] = value.strftime("%Y-%m-%d")
            else:
                treatment_dict[cursor.description[i][0]] = value
        json_object = json.dumps(treatment_dict)
        return Response(json_object, status=200, mimetype='application/json')


@app.route('/treatment/<s_id>', methods=['POST'])
def add_treatment(s_id):
    content = json.loads(request.data)
    sqlcommand = "INSERT INTO b_behandlungen(`b_datevon`,`b_behandlung`, `b_datebis`, `b_infos`, `b_stoecke`, `b_standorte`, `b_user`, `b_von`, `b_bis`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s');" % (
        content["b_datevon"], content["b_behandlungen"], content["b_datebis"], content["b_infos"], s_id,
        content["b_standorte"], content["b_user"], content["b_von"], content["b_bis"])
    cursor = mysql.connect().cursor()
    cursor.execute(sqlcommand)
    cursor.connection.commit()
    json_object = json.dumps({'success': True, 'new_id': cursor.lastrowid})
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/treatment/<b_id>', methods=['DELETE'])
def delete_treatment(b_id):
    cursor = mysql.connect().cursor()
    cursor.execute("DELETE FROM b_behandlungen WHERE b_id = %s", b_id)
    cursor.connection.commit()
    deleted_row_count = cursor.rowcount
    cursor.close()
    if deleted_row_count == 1:
        json_dict = {
            'success': True,
            'message': 'Treatment with id ' + b_id + ' deleted'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=200, mimetype='application/json')
    elif deleted_row_count == 0:
        json_dict = {
            'success': False,
            'message': 'There is no treatment with id ' + b_id
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=404, mimetype='application/json')
    else:
        json_dict = {
            'success': False,
            'message': 'More than one treatment is affected'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=400, mimetype='application/json')


@app.route('/treatment/<b_id>', methods=['PUT'])
def action_treatment(b_id):
    content = json.loads(request.data)
    cur = mysql.connect().cursor()
    cur.execute(
        "UPDATE b_behandlungen SET b_datevon = %s, b_behandlung= %s, b_datebis = %s, b_infos = %s, b_stoecke = %s, b_standorte = %s, b_user = %s,  b_von = %s,  b_bis = %s WHERE b_id = %s",
        (content["b_datevon"], content["b_behandlungen"], content["b_datebis"], content["b_infos"],
         content["b_stoecke"], content["b_standorte"], content["b_user"], content["b_von"], content["b_bis"], b_id))
    cur.connection.commit()
    json_dict = {
        'success': True,
        'message': 'Treatment with id ' + b_id + ' updated'
    }
    json_object = json.dumps(json_dict)
    return Response(json_object, status=200, mimetype='application/json')


# --------------------------------------------------------------------------------------Token----------------------------------------------------

@app.route('/tokens', methods=['GET'])
def get_tokens():
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM t_token")
    r = [dict((cursor.description[i][0], value) for i, value in enumerate(row)) for row in cursor.fetchall()]
    return jsonify(r)


@app.route('/token/<t_id>', methods=['GET'])
def get_token(t_id):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM t_token WHERE t_id= %s", t_id)
    row = cursor.fetchone()
    if row is None:
        json_dict = {
            'statusCode': 404,
            'error': 'Not Found'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=404, mimetype='application/json')
    else:
        token_dict = {}
        for i, value in enumerate(row):
            if isinstance(value, datetime.date):
                token_dict[cursor.description[i][0]] = value.strftime("%Y-%m-%d")
            else:
                token_dict[cursor.description[i][0]] = value
        json_object = json.dumps(token_dict)
        return Response(json_object, status=200, mimetype='application/json')


@app.route('/token', methods=['POST'])
def add_token():
    content = json.loads(request.data)
    sqlcommand = "INSERT INTO t_token(`t_email`,`t_datetime`, `t_id`) VALUES ('%s','%s','%s');" % (
        content["t_email"], content["t_datetime"], content["t_id"])
    cursor = mysql.connect().cursor()
    cursor.execute(sqlcommand)
    cursor.connection.commit()
    json_object = json.dumps({'success': True, 'new_id': cursor.lastrowid})
    return Response(json_object, status=200, mimetype='application/json')


@app.route('/token/<t_id>', methods=['DELETE'])
def delete_token(t_id):
    cursor = mysql.connect().cursor()
    cursor.execute("DELETE FROM t_token WHERE t_id = %s", t_id)
    cursor.connection.commit()
    deleted_row_count = cursor.rowcount
    cursor.close()
    if deleted_row_count == 1:
        json_dict = {
            'success': True,
            'message': 'Token with id ' + t_id + ' deleted'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=200, mimetype='application/json')
    elif deleted_row_count == 0:
        json_dict = {
            'success': False,
            'message': 'There is no token with id ' + t_id
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=404, mimetype='application/json')
    else:
        json_dict = {
            'success': False,
            'message': 'More than one token is affected'
        }
        json_object = json.dumps(json_dict)
        return Response(json_object, status=400, mimetype='application/json')


# --------------------------------------------------------------------------------------Main----------------------------------------------------

if __name__ == '__main__':
    app.run()
